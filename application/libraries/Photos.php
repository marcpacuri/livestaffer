<?php 

class Photos{

	function process($path, $filename){

		$CI =& get_instance();

		# Resize
		$loc = $path . $filename;

        $config['image_library'] = 'gd2';
        $config['source_image'] = $loc;
        $config['maintain_ratio'] = TRUE;
        $config['width'] = 300;
        $config['height'] = 300;
        $CI->load->library('image_lib', $config);

        if (!$CI->image_lib->resize()) {
            echo $CI->image_lib->display_errors();
        } else {
            //create thumbnail
            $CI->load->library('images');

            $original = $filename;
            $new = 'thumb_' . $filename;
            $originalPath = $path;
            $newPath = $path;

            if (!is_dir($newPath)) {
                mkdir($newPath, 0755);
            }

            $CI->images->squareThumb($originalPath . $original, $newPath . $new, 80);
        }

	}

	function bucket_upload($path, $filename, $user_id){
		
		$CI =& get_instance();
		# Check if bucket exists
        $bucket_name = 'livestaffer_profile_pics';
        $CI->load->library('s3');
        $bucket = $CI->s3->getBucket($bucket_name);
        if(!$bucket){
            $CI->s3->putBucket($bucket_name);
        }

        $CI->s3->putObject(file_get_contents($path . '/' . $filename), $bucket_name, $filename);
        $CI->s3->putObject(file_get_contents($path . '/thumb_' . $filename), $bucket_name, $filename);
	}
}

?>