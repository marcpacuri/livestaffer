<div class="container">
	<div class="login well">
		<fieldset>
			<legend>Account Login</legend>

			<form action="<?=site_url('users/login');?>" method="post" class="row-fluid">
			
				<div class="control-group">
					<label for="" class="control-label">Username</label>
					<div class="controls">
						<input type="text" name="username" required class="span12">
					</div>
				</div>

				<div class="control-group">
					<label for="" class="control-label">Password</label>
					<div class="controls">
						<input type="password" name="password" required class="span12">
					</div>
				</div>

				<div class="control-group">
					<label for="" class="control-label"></label>
					<div class="controls text-center">
						<label for="radio_employee" class="radio inline">
							<input type="radio" name="role" value="employee" id="radio_employee" checked> Employee
						</label>
						<label for="radio_employer" class="radio inline">
							<input type="radio" name="role" value="employer" id="radio_employer"> Employer
						</label>
					</div>
				</div>

				<div class="text-center text-error">
					<?= validation_errors();?>
				</div>
				<hr>
				<div class="text-center">
					<button class="btn btn-primary" type="submit">Login</button> or <a href="<?=site_url('register');?>">Register</a>
				</div>
			</form>
		</fieldset>
	</div>
</div>