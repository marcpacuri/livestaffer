<form action="<?=site_url('users/validate');?>" method="post" class="form-horizontal">

	<div class="text-center text-error">
		<?= validation_errors();?>
	</div>

	<div class="control-group">
		<label for="" class="control-label">Email</label>
		<div class="controls">
			<input type="text" name="email" required value="<?=$post['email'];?>">
		</div>
	</div>

	<div class="control-group">
		<label for="" class="control-label">First Name</label>
		<div class="controls">
			<input type="text" name="firstName" required value="<?=$post['firstName'];?>">
		</div>
	</div>

	<div class="control-group">
		<label for="" class="control-label">Last Name</label>
		<div class="controls">
			<input type="text" name="lastName" required value="<?=$post['lastName'];?>">
		</div>
	</div>

	<div class="form-actions">
		<button class="blue btn">Submit</button>
		<a href="<?=site_url('users/skip_validation');?>">Skip</a>
	</div>

</form>