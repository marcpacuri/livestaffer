<fieldset>
	<legend>Registration</legend>

	<form action="<?=site_url('register');?>" method="post" class="form-horizontal">

		<div class="text-center text-error">
			<?= validation_errors();?>
		</div>
		
		<div class="control-group">
			<label for="" class="control-label">Username</label>
			<div class="controls">
				<input type="text" name="username" required value="<?=$post['username'];?>">
			</div>
		</div>

		<div class="control-group">
			<label for="" class="control-label">First Name</label>
			<div class="controls">
				<input type="text" name="firstname" required value="<?=$post['firstname'];?>">
			</div>
		</div>

		<div class="control-group">
			<label for="" class="control-label">Last Name</label>
			<div class="controls">
				<input type="text" name="lastname" required value="<?=$post['lastname'];?>">
			</div>
		</div>

		<div class="control-group">
			<label for="" class="control-label">Email</label>
			<div class="controls">
				<input type="text" name="email" required value="<?=$post['email'];?>">
			</div>
		</div>

		<div class="control-group">
			<label for="" class="control-label">Timezone</label>
			<div class="controls">
				<?=timezone_menu($post['timezone'], NULL, 'timezone');?>
			</div>
		</div>

		<div class="control-group">
			<label for="" class="control-label">Password</label>
			<div class="controls">
				<input type="password" name="password" required>
			</div>
		</div>

		<div class="control-group">
			<label for="" class="control-label">Retype Password</label>
			<div class="controls">
				<input type="password" name="password_conf" required>
			</div>
		</div>

		<div class="control-group">
			<label for="" class="control-label">I'd like to</label>
			<div class="controls">
				<label for="work" class="radio inline">
					<input type="radio" id="work" name="role" value="employee" checked> Work
				</label>

				<label for="hire" class="radio inline">
					<input type="radio" id="hire" name="role" value="employer"> Hire
				</label>
			</div>
		</div>

		<div class="form-actions">
			<button class="btn blue" type="submit">Next</button>
		</div>


	</form>
</fieldset>