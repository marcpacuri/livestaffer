<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<html>
<head>
	<meta charset="utf-8" />
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<title>
		<?php 
		if(isset($title)) echo $title;
		?>
	</title>
	
	<?=$this->load->view('layouts/styles');?>
	
	<input type="hidden" id="environment" value="<?=ENVIRONMENT;?>">
	

</head>

<body class="page-header-fixed">
	<!-- BEGIN HEADER -->
	<div class="header navbar navbar-inverse navbar-fixed-top">
		<!-- BEGIN TOP NAVIGATION BAR -->
		<div class="navbar-inner">
			<div class="container-fluid">
				<!-- HEADER CONTENT GOES HERE -->
				<a class="brand" href="<?=site_url();?>">
					<span style="margin-left: 20px;">LiveStaffer</span>
				</a>

				<?=$this->load->view('layouts/top_nav');?>
			</div> 
		</div>
		<!-- END TOP NAVIGATION BAR -->
	</div>
	<!-- END HEADER -->

	<!-- BEGIN CONTAINER -->  
	<div class="page-container row-fluid">
		
		<?=$this->load->view('layouts/sidebar');?>

		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- PAGE CONTENT GOES HERE -->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<?php if($this->session->userdata('user_id')){?>
						<?=$this->load->view('layouts/breadcrumbs');?>
						<?php } ?>
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- CONTENT BODY GOES HERE >>>> -->

				<?php 
				if(isset($content)) echo $content;
				?>

			</div>
			<!-- END PAGE -->          
		</div>
		<!-- END CONTAINER -->   

		<audio  style="display: none" id="pop_mp3">
	        <source src="<?=base_url('assets/sounds/pop.mp3');?>" type="audio/mpeg" />
        </audio>
		
		<?=$this->load->view('layouts/authorize_notification');?>
		<?=$this->load->view('layouts/footer');?>
		<?=$this->load->view('layouts/scripts');?>
		
	</body>
</html>