<!-- <div class="navbar hor-menu hidden-phone hidden-tablet">
	<div class="navbar-inner">
		<ul class="nav">

		</ul>

	</div>
</div> -->

<!-- BEGIN RESPONSIVE MENU TOGGLER -->
<a href="javascript:;" class="btn-navbar collapsed" data-toggle="collapse" data-target=".nav-collapse">
<img src="<?=base_url('assets/metronic/img/menu-toggler.png');?>" alt="" />
</a>          
<!-- END RESPONSIVE MENU TOGGLER --> 
<?php if($this->session->userdata('user_id')){?>
<!-- BEGIN TOP NAVIGATION MENU -->              
<ul class="nav pull-right">

	
	
	<li class="dropdown" id="header_notification_bar" style="outline: 0px; box-shadow: rgba(102, 188, 230, 0) 0px 0px 13px; outline-offset: 20px;">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" id="notification_btn">
		<i class="icon-warning-sign"></i>
		<span class="badge hide" id="notify-badge">!</span>
		</a>
		<ul class="dropdown-menu extended notification">

		
			<li>
				<p>You have <span id="notify_number">no</span> new notifications</p>
			</li>
			<li>
				<div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 250px;"><ul class="dropdown-menu-list scroller" style="height: 250px; overflow: hidden; width: auto;" id="notification-list">
					<div id="notify" class="hide">
						<li>
							<a href="#" class='link'>
							<span class="label label-success"><i class="icon-bullhorn"></i></span>
							<span class="message"></span> 
							<span class="time">Just now</span>
							</a>
						</li>
					</div>

				</ul><div class="slimScrollBar ui-draggable" style="background-color: rgb(161, 178, 189); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-top-left-radius: 7px; border-top-right-radius: 7px; border-bottom-right-radius: 7px; border-bottom-left-radius: 7px; z-index: 99; right: 1px; height: 146.02803738317758px; background-position: initial initial; background-repeat: initial initial;"></div><div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-top-left-radius: 7px; border-top-right-radius: 7px; border-bottom-right-radius: 7px; border-bottom-left-radius: 7px; background-color: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px; background-position: initial initial; background-repeat: initial initial;"></div></div>
			</li>
			<li class="external">
				<a href="#">See all notifications <i class="m-icon-swapright"></i></a>
			</li>
		</ul>
	</li>

	<!-- BEGIN USER LOGIN DROPDOWN -->
	<li class="dropdown user top_nav">

		<a href="#" class="dropdown-toggle" data-toggle="dropdown">
				<img alt="" src="<?=$this->session->userdata('profile_pic_thumb');?>" class="profile_pic"/>
	
			
			<span class="username">
				<?= $this->session->userdata('firstname') . ' ' . $this->session->userdata('lastname');?>
			</span>
			<i class="icon-angle-down"></i>
		</a>
		<ul class="dropdown-menu">
			<?php if($this->session->userdata('role') == 'employee'){?>
			<li><a href="<?=site_url('employees/account');?>"><i class="icon-user"></i> Account Settings</a></li>
			<?php } else {?>
			<li><a href="<?=site_url('employers/account');?>"><i class="icon-user"></i> Account Settings</a></li>
			<?php } ?>
			<li class="divider"></li>
			<li><a href="<?=site_url('users/logout');?>"><i class="icon-key"></i> Log Out</a></li>
		</ul>
	</li>
	<!-- END USER LOGIN DROPDOWN -->
</ul>
<?php } ?>