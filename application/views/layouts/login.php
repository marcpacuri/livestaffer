<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<html>
<head>
	<meta charset="utf-8" />
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<title>
		<?php 
		if(isset($title)) echo $title;
		?>
	</title>

	<!-- BEGIN GLOBAL MANDATORY STYLES -->
	<?=$this->load->view('layouts/styles');?>
	<link rel="stylesheet" type="text/css" href="<?=base_url('assets/metronic/plugins/select2/select2_metro.css');?>" />
	<link href="<?=base_url('assets/metronic/css/pages/login-soft.css');?>" rel="stylesheet" type="text/css"/>
	
</head>

<body class="login">

	<!-- BEGIN LOGO -->
	<div class="logo">
		<!-- <img src="assets/img/logo-big.png" alt="" />  -->
	</div>
	<!-- END LOGO -->
	<!-- BEGIN LOGIN -->
	<div class="content">
		<!-- BEGIN LOGIN FORM -->
		<form action="<?=site_url('users/login');?>" method="post" class="form-vertical login-form">
			<h3 class="form-title">Login to your account</h3>
			<div class="alert alert-error hide">
				<button class="close" data-dismiss="alert"></button>
				<span>Enter any username and password.</span>
			</div>
			<div class="control-group">
				<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
				<label class="control-label visible-ie8 visible-ie9">Username</label>
				<div class="controls">
					<div class="input-icon left">
						<i class="icon-user"></i>
						<input class="m-wrap placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" name="username" required/>
					</div>
				</div>
			</div>

			

			<div class="control-group">
				<label class="control-label visible-ie8 visible-ie9">Password</label>
				<div class="controls">
					<div class="input-icon left">
						<i class="icon-lock"></i>
						<input class="m-wrap placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password" required/>
					</div>
				</div>
			</div>

			<div class="control-group">
					<label for="" class="control-label"></label>
					<div class="controls text-center">
						<label for="radio_employee" class="radio inline">
							<input type="radio" name="role" value="employee" id="radio_employee" checked> Employee
						</label>
						<label for="radio_employer" class="radio inline">
							<input type="radio" name="role" value="employer" id="radio_employer"> Employer
						</label>
					</div>
				</div>



			<div class="text-center text-error">
				<?=validation_errors();?>
			</div>
			<div class="form-actions">
				<label class="checkbox">
				<input type="checkbox" name="remember" value="1"/> Remember me
				</label>
				<button type="submit" class="btn blue pull-right">
				Login <i class="m-icon-swapright m-icon-white"></i>
				</button>            
			</div>
			
		</form>
		<!-- END LOGIN FORM -->

	</div>

		
	<?=$this->load->view('layouts/scripts');?>
	<script src="<?=base_url('assets/metronic/plugins/jquery-validation/dist/jquery.validate.min.js');?>" type="text/javascript"></script>
	<script src="<?=base_url('assets/metronic/plugins/backstretch/jquery.backstretch.min.js');?>" type="text/javascript"></script>
	<script type="text/javascript" src="<?=base_url('assets/metronic/plugins/select2/select2.min.js');?>"></script>
	<script src="<?=base_url('assets/metronic/scripts/login-soft.js');?>" type="text/javascript"></script> 

	<script>
		jQuery(document).ready(function() {     
		  Login.init();
		});
	</script>
   

</body>
</html>