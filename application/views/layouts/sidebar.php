<!-- BEGIN SIDEBAR -->
<div class="page-sidebar nav-collapse collapse">
	<!-- PAGE SIDEBAR CONTENT GOES HERE -->
	<ul class="page-sidebar-menu">
		<li>
			<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
			<div class="sidebar-toggler hidden-phone">
			</div>
			<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
		</li>

		<li>
			<!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
			<form class="sidebar-search">
				<div class="input-box">
					<a href="javascript:;" class="remove">
					</a>
					<input type="text" placeholder="Search..." />        
					<input type="button" class="submit" value=" " />
				</div>
			</form>
			<!-- END RESPONSIVE QUICK SEARCH FORM -->
		</li> 
		
		<li class="start active">
				<a href="#">
					<i class="icon-barcode"></i><span class="title">Dashboard</span>      
					<span class="arrow open"></span>
					<span class="selected"></span>
				</a>
				<ul class="sub-menu">
					<li>
						<a href="<?=site_url();?>">Dashboard</a>
					</li>
				</ul>

			</a>
			
		</li>

		<li class="start">
				<a href="#">
					<i class="icon-user"></i><span class="title">Account</span>      
					<span class="arrow"></span>
					<span class="selected"></span>
				</a>

				

				<ul class="sub-menu">

				<?php if($this->session->userdata('role') == 'employer'){?>
					<li><a href="<?=site_url('employers/account');?>">Edit Details</a></li>
					<li><a href="<?=site_url('employers/password');?>">Change Password</a></li>
					<li><a href="<?=site_url('employers/notification_settings');?>">Notification Settings</a></li>
				<?php } else {?>
					<li><a href="<?=site_url('employees/account');?>">Edit Details</a></li>
					<li><a href="<?=site_url('employees/password');?>">Change Password</a></li>
					<li><a href="<?=site_url('employees/skills');?>">Skills & Experience</a></li>
					<li><a href="<?=site_url('employees/notification_settings');?>">Notification Settings</a></li>
				<?php } ?>
					
				</ul>

			</a>
			
		</li>

	</ul>
</div>
<!-- END SIDEBAR -->