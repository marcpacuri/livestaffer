<!-- BEGIN CORE PLUGINS -->
<script rel="text/javascript" src="<?=base_url('assets/metronic/plugins/jquery-1.10.1.min.js');?>"></script>
<script src="<?=base_url('assets/metronic/plugins/jquery-migrate-1.2.1.min.js');?>" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="<?=base_url('assets/metronic/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js');?>" type="text/javascript"></script>      
<script src="<?=base_url('assets/bootstrap/js/bootstrap.min.js');?>" type="text/javascript"></script>
<!--[if lt IE 9]>
<script src="<?=base_url('assets/metronic/plugins/excanvas.min.js');?>"></script>
<script src="<?=base_url('assets/metronic/plugins/respond.min.js');?>"></script>  
<![endif]-->   
<script src="<?=base_url('assets/metronic/plugins/jquery-slimscroll/jquery.slimscroll.min.js');?>" type="text/javascript"></script>
<script src="<?=base_url('assets/metronic/plugins/jquery.blockui.min.js');?>" type="text/javascript"></script>  
<script src="<?=base_url('assets/metronic/plugins/jquery.cookie.min.js');?>" type="text/javascript"></script>
<script src="<?=base_url('assets/metronic/plugins/uniform/jquery.uniform.min.js');?>" type="text/javascript" ></script>
<script type="text/javascript" src="<?=base_url('assets/metronic/plugins/ckeditor/ckeditor.js');?>"></script>  
<script type="text/javascript" src="<?=base_url('assets/metronic/scripts/app.js');?>"></script>  
<!-- END CORE PLUGINS -->


<!-- TABLE -->
<script type="text/javascript" src="<?=base_url('assets/metronic/plugins/select2/select2.min.js');?>"></script>
<script type="text/javascript" src="<?=base_url('assets/metronic/plugins/data-tables/jquery.dataTables.js');?>"></script>
<script type="text/javascript" src="<?=base_url('assets/metronic/plugins/data-tables/DT_bootstrap.js');?>"></script>
<script src="<?=base_url('assets/metronic/scripts/table-editable.js');?>"></script>   
<!-- END TABLE -->


<!-- MY PLUGINS -->
<?php if(ENVIRONMENT == 'development'){?>
<script src="http://localhost:8080/socket.io/socket.io.js"></script>
<?php } else { ?>
<script src="http://live_signal.jit.su/socket.io/socket.io.js"></script>
<?php } ?>


<script type="text/javascript" src="<?=base_url('assets/js/socket.js');?>"></script> 
<script type="text/javascript" src="<?=base_url('assets/js/moment.min.js');?>"></script>  
<script type="text/javascript" src="<?=base_url('assets/metronic/plugins/gritter/js/jquery.gritter.js');?>"></script>


<?php if(!isset($login)){?>
<script type="text/javascript" src="<?=base_url('assets/js/script.js');?>"></script>  
<?php } ?>



<?php if($this->session->userdata('role') == 'employee'){?>
<script src="<?=base_url('assets/js/employees.js');?>"></script>
<?php } else{ ?>
<script src="<?=base_url('assets/js/employers.js');?>"></script>
<?php } ?>


<!-- END MY PLUGINS -->


<?php 

	if(isset($js)){
		foreach($js as $i){?>
			<script rel="text/javascript" src="<?=$i;?>"></script>
		<?php }
	}

?>

<script>
	jQuery(document).ready(function() {    
	   App.init(); // initlayout and core plugins
	   // TableEditable.init();
	   <?php 

	   		if(isset($init)){
	   			switch($init){
	   				case 'TableEditable':
	   					echo "TableEditable.init();";
	   					break;
	   			}
	   		}

	   ?>
	});
</script>