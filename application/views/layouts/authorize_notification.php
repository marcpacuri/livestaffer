<div id="authorize_modal" class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Notification Alerts</h3>
    </div>
    <div class="modal-body">
        <p>Please allow us to send you browser notifications so that you don't miss any messages. Click the Authorize button below </p>
    </div>
    <div class="modal-footer">
        <button class="btn red" data-dismiss="modal" aria-hidden="true">Close</button>
         <button class="btn blue" data-dismiss="modal" aria-hidden="true" id="authorize">Authorize</button>
    </div>
</div>
