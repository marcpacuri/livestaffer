<!-- BEGIN PAGE TITLE & BREADCRUMB-->   
<h3 class="page-title">
	<?php 
	if(isset($page_title)){
		echo $page_title;
	} else{
		echo 'Page Title';
	}
	?>
	<small>
		<?php 
		if(isset($page_sub_title)){
			echo $page_sub_title;
		} else{
			echo 'sub title';
		}
		?>
	</small>
</h3>
<ul class="breadcrumb">
	<li>
		<i class="icon-home"></i>
		<a href="#">Home</a> 
		<span class="icon-angle-right"></span>
	</li>
	<li><a href="#">Dashboard</a></li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB--> 