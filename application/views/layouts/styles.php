<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link rel="stylesheet" href="<?=base_url('assets/metronic/plugins/bootstrap/css/bootstrap.min.css');?>">
<link rel="stylesheet" href="<?=base_url('assets/metronic/plugins/bootstrap/css/bootstrap-responsive.min.css');?>">
<link href="<?=base_url('assets/metronic/plugins/font-awesome/css/font-awesome.min.css');?>" rel="stylesheet" type="text/css"/>
<link href="<?=base_url('assets/metronic/css/style-metro.css');?>" rel="stylesheet" type="text/css"/>
<link href="<?=base_url('assets/metronic/css/style.css');?>" rel="stylesheet" type="text/css"/>
<link href="<?=base_url('assets/metronic/css/style-responsive.css');?>" rel="stylesheet" type="text/css"/>
<link href="<?=base_url('assets/metronic/css/themes/default.css');?>" rel="stylesheet" type="text/css"/>
<link href="<?=base_url('assets/metronic/plugins/uniform/css/uniform.default.css');?>" rel="stylesheet" type="text/css"/>
<link href="<?=base_url('assets/css/style.less');?>" rel="stylesheet/less" tkype="text/css"/>
<link href="<?=base_url('assets/metronic/plugins/gritter/css/jquery.gritter.css');?>" rel="stylesheet" type="text/css"/>
<script rel="text/javascript" src="<?=base_url('assets/js/less.js');?>"></script>
<!-- END GLOBAL MANDATORY STYLES -->

<?php 

	if(isset($css)){
		foreach($css as $i){?>
			<link href="<?=$i;?>" rel="stylesheet" type="text/css"/>
		<?php }
	}

?>

<script>
var site_url = "<?=site_url();?>";
var base_url = "<?=base_url();?>";
</script>