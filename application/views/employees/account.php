<div class="">
	
	<input type="hidden" value="<?=$this->session->userdata('user_id');?>" id="user_id">

	<div class="">
		<fieldset class="form-horizontal">
		    <legend>Edit Account</legend>
			
			<form action="<?=site_url('employees/account');?>" method="post">
				<div class="text-center text-error">
			<?= validation_errors();?>
		</div>
		
		<div class="control-group">
			<label for="" class="control-label">Username</label>
			<div class="controls">
				<input type="text" name="username" required value="<?=$post['username'];?>" disabled>
			</div>
		</div>

		<div class="control-group">
			<label for="" class="control-label">First Name</label>
			<div class="controls">
				<input type="text" name="firstname" required value="<?=$post['firstname'];?>">
			</div>
		</div>

		<div class="control-group">
			<label for="" class="control-label">Last Name</label>
			<div class="controls">
				<input type="text" name="lastname" required value="<?=$post['lastname'];?>">
			</div>
		</div>

		<div class="control-group">
			<label for="" class="control-label">Email</label>
			<div class="controls">
				<input type="text" name="email" required value="<?=$post['email'];?>" disabled>
			</div>
		</div>

		<div class="control-group">
			<label for="" class="control-label">Timezone</label>
			<div class="controls">
				<?=timezone_menu($post['timezone'], NULL, 'timezone');?>
			</div>
		</div>

		<div class="control-group">
			<label for="" class="control-label">Profile Picture</label>
			<div class="controls">
				
				<?php if($post['profile_pic']){?>
					<div style="margin-bottom: 20px;">
						<img src="<?=base_url('assets/profile_pic/' . $post['profile_pic']);?>" alt="" class="img-polaroid" style="width: 300px;">
					</div>

					<div style="margin-bottom: 20px;">
						<a href="<?=site_url('employees/delete_picture');?>" class="delete-this">
							<i class="icon-trash"></i> Delete Picture
						</a>
					</div>
				
				<?php } ?>
			
				<input type="file" id="profile_pic">
				
				
			</div>
		</div>

		<div class="form-actions">
			<button class="btn blue" type="submit">Save</button>
		</div>
			</form>

		</fieldset>

	</div>

</div>