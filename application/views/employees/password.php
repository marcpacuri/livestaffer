<div class="">



	<div class="span9">
		<fieldset class="form-horizontal">
		    <legend>Change Password</legend>
			
			<form action="<?=site_url('employees/password');?>" method="post">
				<div class="text-center text-error">
					<?= validation_errors();?>
				</div>

				<div class="control-group">
					<label for="" class="control-label">Old Password</label>
					<div class="controls">
						<input type="password" name="old_password" required>
					</div>
				</div>
				
				<div class="control-group">
					<label for="" class="control-label">Password</label>
					<div class="controls">
						<input type="password" name="password" required>
					</div>
				</div>

				<div class="control-group">
					<label for="" class="control-label">Retype Password</label>
					<div class="controls">
						<input type="password" name="password_conf" required>
					</div>
				</div>

				<div class="form-actions">
					<button class="btn btn-primary" type="submit">Save</button>
				</div>
			</form>

		</fieldset>

	</div>

</div>