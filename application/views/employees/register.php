	<form action="<?=site_url('employees/register');?>" method="post" class="form-horizontal">
<div class="text-center text-error">
			<?= validation_errors();?>
		</div>
<fieldset class="span6">
	<legend>Registration</legend>
		
		<div class="control-group">
			<label for="" class="control-label">Username</label>
			<div class="controls">
				<input type="text" name="username" required value="<?=$post['username'];?>">
			</div>
		</div>

		<div class="control-group">
			<label for="" class="control-label">First Name</label>
			<div class="controls">
				<input type="text" name="firstname" required value="<?=$post['firstname'];?>">
			</div>
		</div>

		<div class="control-group">
			<label for="" class="control-label">Last Name</label>
			<div class="controls">
				<input type="text" name="lastname" required value="<?=$post['lastname'];?>">
			</div>
		</div>

		<div class="control-group">
			<label for="" class="control-label">Email</label>
			<div class="controls">
				<input type="text" name="email" required value="<?=$post['email'];?>">
			</div>
		</div>

		<div class="control-group">
			<label for="" class="control-label">Timezone</label>
			<div class="controls">
				<?=timezone_menu($post['timezone'], NULL, 'timezone');?>
			</div>
		</div>

		<div class="control-group">
			<label for="" class="control-label">Password</label>
			<div class="controls">
				<input type="password" name="password" required>
			</div>
		</div>

		<div class="control-group">
			<label for="" class="control-label">Retype Password</label>
			<div class="controls">
				<input type="password" name="password_conf" required>
			</div>
		</div>

	
</fieldset>

<fieldset class="form-horizontal span5">
	<legend>Paypal Account</legend>
		

		<div class="control-group">
			<label for="" class="control-label">Paypal Email</label>
			<div class="controls">
				<input type="text" name="paypal_email" required value="<?=$post['paypal_email'];?>">
			</div>
		</div>

		<div class="control-group">
			<label for="" class="control-label">First Name</label>
			<div class="controls">
				<input type="text" name="firstName" required value="<?=$post['firstName'];?>">
			</div>
		</div>

		<div class="control-group">
			<label for="" class="control-label">Last Name</label>
			<div class="controls">
				<input type="text" name="lastName" required value="<?=$post['lastName'];?>">
			</div>
		</div>

		<div class="control-group">
			<label for="" class="control-label"></label>
			<div class="controls">
				 <label class="checkbox">
			      <input type="checkbox" id="use_reg_details"> Use Registration Details
			    </label>
			</div>
		</div>

		
</fieldset>
<div class="clear"></div>
<div class="form-actions">
	<button class="btn blue" type="submit">Submit</button>
</div>

</form>

	