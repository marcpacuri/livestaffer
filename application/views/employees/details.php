
<h2><?=$user['firstname'] . ' ' . $user['lastname'];?></h2>

<table class="table table-striped">
	<tr>
		<tr>
			<td class="text-right">Skills:</td>
			<td><?=$user['skills'];?></td>
		</tr>
		<tr>
			<td class="text-right">Experience:</td>
			<td><?=$user['experience'];?> Year(s)</td>
		</tr>


		<tr>
			<td>Rate Per Hour</td>
			<td>
				$<?=$user['rate'];?> / hour
			</td>
		</tr>


		<tr>
			<td>Rating</td>
			<td></td>
		</tr>

		<tr>
			<td>Previous Projects</td>
			<td>
				
			</td>
		</tr>
	</tr>
</table>

<?php if(isset($job_id)){?>
<a href="<?=site_url('jobs/workroom/'. $job_id . '/' . $workroom);?>" class="btn btn-success">Interview</a>
<?php } ?>

