<div class="">
	<div class="span9">
		<fieldset class="form-horizontal">
			<legend>Update Skills</legend>
			
			<form action="<?=site_url('employees/skills');?>" method="post">
			<div class="control-group">
				<label for="" class="control-label">Experience <sub>(years)</sub></label>
				<div class="controls">
					<input type="text" name="experience" value="<?=$user['experience'];?>" required>
				</div>
			</div>
			<div class="control-group">
				<label for="" class="control-label">Skills</label>
				<div class="controls">
					<?php foreach($skills as $i => $v){?>
					<label class="checkbox" for="skill_<?=$i;?>">
						<input type="checkbox" value="<?=$v;?>" name="skills[]" id="skill_<?=$i;?>" <?php if(in_array($v, $user['skills'])) echo 'checked';?>/> <?=$v;?>
					</label>
					<?php } ?>
				</div>
			</div>

			<div class="control-group">
				<label for="" class="control-label">Rate <sub>(per hour)</sub></label>
				<div class="controls">
					<input type="text" name="rate" value="<?=$user['rate'];?>" required>
				</div>
			</div>

			<div class="form-actions">
				<button class="btn btn-primary">Save</button>
			</div>
			</form>
		</fieldset>
	</div>
</div>