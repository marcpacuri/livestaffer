<div class="span3 account-side">
	<ul class="nav nav-list">
	  <li class="nav-header">Account Settings</li>
	  <li><a href="<?=site_url('employees/account');?>">Edit Details</a></li>
	  <li><a href="<?=site_url('employees/password');?>">Change Password</a></li>
	  <li><a href="<?=site_url('employees/skills');?>">Skills & Experience</a></li>
	  <li><a href="<?=site_url('employees/notification_settings');?>">Notification Settings</a></li>
	</ul>
</div>