<div class="span3 account-side">
	<ul class="nav nav-list">
	  <li class="nav-header">Account Settings</li>
	  <li><a href="<?=site_url('employers/account');?>">Edit Details</a></li>
	  <li><a href="<?=site_url('employers/password');?>">Change Password</a></li>
	  <li><a href="<?=site_url('employers/notification_settings');?>">Notification Settings</a></li>
	</ul>
</div>