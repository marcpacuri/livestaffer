<div class="dashboard">
	
	<div class="">
		<div class="span9">
			<ul class="nav nav-tabs">
			  <li class="active"><a href="#open" data-toggle="tab">Open Jobs</a></li>
			  <li><a href="#ongoing" data-toggle="tab">Ongoing</a></li>
			  <li><a href="#complete" data-toggle="tab">Complete</a></li>
			</ul>

			<div class="tab-content">
			  <div class="tab-pane active" id="open">
			  	
			  	<ul class="unstyled list-box">
			  		
			  		<?php if($open_jobs){?>
					
				  		<?php foreach($open_jobs as $job){?>

				  		<li>
				  			<div class="date pull-right"><small><?=$job['created_at'];?></small></div>
				  			<h4>
				  				<a href="<?=site_url('jobs/details/' . $job['id']);?>"><?=$job['title'];?></a>
				  			</h4>

				  			<p>
				  				<?= $job['description'];?>
				  			</p>
				  		</li>
				  		<?php } ?>
			  		<?php } else {?>
						<h4 class="no-items text-center">No Items</h4>
			  		<?php } ?>
			  		
			   
			  	</ul>

			  	<!-- <button class="btn btn-block btn-primary">Load More</button> -->

			  </div>


			  <div class="tab-pane" id="ongoing">
			  	<ul class="unstyled list-box">
			  	<?php if($in_progress){?>
					
				  		<?php foreach($in_progress as $job){?>

				  		<li>
				  			<div class="date pull-right"><small><?=$job['created_at'];?></small></div>
				  			
				  			<h4>
				  				<a href="<?=site_url('jobs/details/' . $job['id']);?>"><?=$job['title'];?></a>
				  			</h4>

				  			<div class="pull-right">
				  				<a href="<?=site_url('workrooms/room?openRoom='. $job['workroom']);?>" class="btn blue">Workroom</a>
				  			</div>

				  			<p>
				  				<?= $job['description'];?>
				  			</p>
				  		</li>
				  		<?php } ?>
				  	</ul>
			  		<?php } else {?>
						<h4 class="no-items text-center">No Items</h4>
			  		<?php } ?>
			  </div>
			  <div class="tab-pane" id="complete">
			  	<ul class="unstyled list-box">
			  	<?php if($complete){?>
					
				  		<?php foreach($complete as $job){?>

				  		<li>
				  			<div class="date pull-right"><small><?=$job['created_at'];?></small></div>
				  			
				  			<h4>
				  				<a href="<?=site_url('jobs/details/' . $job['id']);?>"><?=$job['title'];?></a>
				  			</h4>

				  		

				  			<p>
				  				<?= $job['description'];?>
				  			</p>
				  		</li>
				  		<?php } ?>
				  	</ul>
			  		<?php } else {?>
						<h4 class="no-items text-center">No Items</h4>
			  		<?php } ?>
			  	</ul>
			  </div>
			  </div>
			</div>
		</div>

		<?=$this->load->view('employers/side');?>

	</div>
	
</div>