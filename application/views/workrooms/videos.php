 <div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption"><i class="icon-video"></i>Video</div>
    </div>

    <div class="portlet-body">
       <button id="btn-share-call"  class="btn green"><i class="icon-phone"></i> Call</button>
       <button id="btn-share-video"  class="btn green"><i class="icon-film"></i> Video</button>
       <button id="end_call" class="btn red">Hang Up</button>
       <button id="btn-share-screen" disabled class="hide">Share Screen</button>
      
        <h3 class="hide" id="call_status">Calling...</h3>
       <div class="videos-container"></div>

	<!-- <div class="videos">	
		<div id="localVideo" class="localVideo"></div>
	     <div id="remotesVideos" class="remotesVideos"></div>
    </div>	 -->
        <div class="clear"></div>

    </div>
</div>

<div id="answer" class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Call</h3>
    </div>
    <div class="modal-body">
        <p>A user is calling you. </p>
    </div>
    <div class="modal-footer">
        <button class="btn red" data-dismiss="modal" aria-hidden="true" id="decline_call">Decline</button>
         <button class="btn blue" data-dismiss="modal" aria-hidden="true" id="answer_call">Answer</button>
         <button class="btn blue" data-dismiss="modal" aria-hidden="true" id="answer_video_call">Answer Video Call</button>
    </div>
</div>

