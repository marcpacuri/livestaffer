<input type="hidden" value="<?=$this->session->flashdata('new_room');?>" id="new_workroom">
<input type="hidden" value="<?=$workroom['hash'];?>" id="workroom_hash">
<input type="hidden" value="<?=$workroom['id'];?>" id="workroom_id">
<input type="hidden" value="<?=$workroom['job_id'];?>" id="job_id">
<input type="hidden" value="<?=$workroom['employee_id'];?>" id="employee_id">
<input type="hidden" value="<?=$workroom['employer_id'];?>" id="employer_id">
<input type="hidden" value="<?=$this->session->userdata('role');?>" id="role">

<input type="hidden" value="<?=$workroom['sender']['username'];?>" id="username">
<input type="hidden" value="<?=$workroom['receiver']['username'];?>" id="recipient">

<div id="room_notify"></div>

<div class="row-fluid">




    <div class="span6">
      <?=$this->load->view('workrooms/online');?>
      <div class="portlet box blue">
        <div class="portlet-title">
            <div class="caption"><i class="icon-comment"></i>Chat</div>
        </div>

        <div class="portlet-body" style="padding: 0">

            <div class="chat-output-panel slimScrollDiv" style="height: 300px; overflow-y: scroll; padding: 10px;">

                <h5 class="text-center" style="margin-bottom: 30px; display:none" id="chat_preloader">Loading...</h5>

                <div id="message_in" class="hide">
                    <li class="in">
                        <img class="avatar" alt="" src="<?=$this->session->userdata('profile_pic_thumb');?>">
                        <div class="message">
                            <span class="arrow"></span>
                            <a href="#" class="name"><?=$workroom['sender']['firstname'] . ' ' . $workroom['sender']['lastname'] ;?></a>
                            <span class="datetime">at  11:09</span>
                            <span class="body">
                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
                            </span>
                        </div>
                    </li>
                </div>

                <div id="message_out" class="hide">
                    <li class="out">
                        <img class="avatar" alt="" src="<?=$workroom['receiver']['profile_pic'];?>">
                        <div class="message">
                            <span class="arrow"></span>
                            <a href="#" class="name"><?=$workroom['receiver']['firstname'] . ' ' . $workroom['receiver']['lastname'];?></a>
                            <span class="datetime">at Jul 25, 2012 11:09</span>
                            <span class="body">
                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
                            </span>
                        </div>
                    </li>
                </div>

                <ul class="chats"></ul>

            </div>


        </div>
    </div>


    <form action="#" id="chat_form">

        <textarea name="message" id="chat-input-box" cols="" rows="5" style="width: 98%"></textarea>
        <input type="file" id="share-file" disabled>
        <div class="pull-right">
            <!-- <button class="btn blue" type="submit">Send</button> -->
            <input type="hidden" id="chat_index" value="0">
        </div>
    </form>

</div>

<div class="span6">

    <div class="">

        <div class="dashboard-stat green timer-log">

           <input type="hidden" id="rate" value="<?=$workroom['rate'];?>">
           <input type="hidden" id="total_time" value="<?=$log['total_time'];?>">
           <div class="visual">
            <i class="icon-time"></i>
        </div>

        <div class="timer-log" style="display: inline-block">
            <div class="number">
                <span id="hours"><?=$log['hours'];?></span> :
                <span id="minutes"><?=$log['minutes'];?></span> :
                <span id="seconds"><?=$log['seconds'];?></span>
            </div>
            <div class="desc">Timer</div>
        </div>

        <div class="details">
            <div class="number">$ <span id="realtime_cost"><?=$log['cost'];?></span></div>
            <div class="desc">Realtime Cost</div>
        </div>
        
        <div class="more">
            <button id="start_time" class="btn blue mini" onclick="start_time()">Start</button>
            <button id="stop_time" class="btn red mini" onclick="stop_time()">Pause</button>
            <?php if($this->session->userdata('role') == 'employer'){?>
            <a href="<?=site_url('workrooms/complete/' . encrypt_id($workroom['id']));?>" class="btn yellow mini pull-right" onclick="stop_time()">Complete Job</a>
            <?php } ?>
        </div>

    </div>


    <?=$this->load->view('workrooms/video_simple');?>
    <?=$this->load->view('workrooms/files');?>


</div>
</div>

</div>

