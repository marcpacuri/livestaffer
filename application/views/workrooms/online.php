<div class="responsive span6" data-tablet="span6" data-desktop="span3">
    <div class="dashboard-stat blue">
        <div class="visual">
            <?php if($this->session->userdata('profile_pic_thumb')){?>
                 <img src="<?=$this->session->userdata('profile_pic_thumb');?>" alt="" class="online-avatar">
            <?php } else {?>
                    <i class="icon-comments"></i>
            <?php } ?>
           
            
        </div>
        <div class="details">
            <div class="number">
                <?=$workroom['sender']['firstname'];?>
            </div>
            <div class="desc">                           
                <span class="badge badge-success">online</span> 

            </div>
        </div>
              
    </div>
</div>

<div class="responsive span6" data-tablet="span6" data-desktop="span3">
    <div class="dashboard-stat blue">
        <div class="visual">
             <?php if($workroom['receiver']['profile_pic']){?>
                 <img src="<?=$workroom['receiver']['profile_pic'];?>" alt="" class="online-avatar">
            <?php } else {?>
                    <i class="icon-comments"></i>
            <?php } ?>
        </div>
        <div class="details">
            <div class="number">
                 <?=$workroom['receiver']['firstname'];?>
            </div>
            <div class="desc"> 
               <span class="badge badge-success hide status-badge-online">online</span>                          
               <span class="badge status-badge-offline">offline</span> 
            </div>
        </div>
             
    </div>
</div>