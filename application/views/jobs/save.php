<fieldset>
	<legend>Job Details</legend>
	
	<form action="<?=site_url('jobs/save/' . $id);?>" method="post" class="form-horizontal">
		
		<div class="text-center">
			<?= validation_errors();?>
		</div>

		<div class="control-group">
			<label for="" class="control-label">Title</label>
			<div class="controls">
				<input type="text" name="title" value="<?=$job['title'];?>">
			</div>
		</div>
		<div class="control-group">
			<label for="" class="control-label">Description</label>
			<div class="controls">
				<textarea name="description" id="" cols="30" rows="5"><?=$job['description'];?></textarea>
			</div>
		</div>

		<div class="control-group">
			<label for="" class="control-label">Budget <sub>(per hour)</sub></label>
			<div class="controls">
				<input type="text" name="budget" value="<?=$job['budget'];?>">
			</div>
		</div>

		<div class="control-group">
			<label for="" class="control-label">Experience <sub>(years)</sub></label>
			<div class="controls">
				<input type="text" name="experience" value="<?=$job['experience'];?>">
			</div>
		</div>

		<div class="control-group">
			<label for="" class="control-label">Skills</label>
			<div class="controls">
				<?php foreach($skills as $i => $v){?>
					<label for="skill_<?=$i;?>" class="checkbox">
						<input type="checkbox" name="skills[]" value="<?=$v;?>" id="skill_<?=$i;?>" <?php if(in_array($v, $job['skills'])) echo 'checked';?>> <?= $v;?>
					</label>
				<?php } ?>
			</div>
		</div>

		<div class="form-actions">
			<button class="btn blue" type="submit">Save</button>
		</div>
	</form>
</fieldset>