<div id="apply_modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">Apply</h3>
  </div>
  <div class="modal-body">
    <form action="<?=site_url('jobs/apply');?>" class="form-horizontal" method="post">
      
      <input type="hidden" name="id" id="apply_id">
      <div class="control-group">
        <label for="" class="control-label">Bid Description</label>
        <div class="controls">
          <textarea name="bid_desc" id="" cols="30" rows="5" required></textarea>
        </div>
      </div>

      <input type="submit" class="hide" id="apply_submit_btn">

    </form>
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
    <button class="btn btn-primary" onclick="$('#apply_submit_btn').trigger('click');">Submit</button>
  </div>
</div>