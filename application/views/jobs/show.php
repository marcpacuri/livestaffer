<input type="hidden" id="new_job" value="<?=$this->session->flashdata('new_job');?>">
<input type="hidden" id="apply_broadcast" value="<?=$this->session->flashdata('apply_broadcast');?>">
<div class="row-fluild">
	<div class="span9">
		<h3>
			<?=$job['title'];?>
			<small class="pull-right"><?= $job['created_at'];?></small>
		</h3>

		<p>
			<?= $job['description'];?>
		</p>

		<table class="table">
			<tr>
				<td class="text-right">Required Skills</td>
				<td>
					<?php 
						foreach($job['skills'] as $i => $v){
							echo $v . ', ';
						}
					?>
				</td>
			</tr>

			<tr>
				<td class="text-right">Budget</td>
				<td>$ <?=$job['budget'];?> / hour</td>
			</tr>
		</table>

		<?php if($this->session->userdata('role') == 'employer'){?>
		<a href="<?=site_url('jobs/save/' . $job['id']);?>" class="btn">Edit</a>
		<!-- <a href="#" class="btn">Delete</a>	 -->
		<?php } else{ ?>
		
		<?php if(isset($apply) && $apply){?>
		<a href="<?=site_url('jobs/withdraw/' . $job['id']);?>" class="btn btn-danger">Withdraw</a>
		<?php } else{?>
		<a href="#apply_modal" class="btn btn-success" onclick="$('#apply_id').val('<?=$job['id'];?>')" data-toggle="modal">Apply</a>
		<?php } ?>

		
		
		<?php } ?>

		<h3 class="text-center">These Workers are Ready to Start Right Now</h3>

		<div class="worker-list">

			<?php foreach($applicants as $i => $v){?>
			<ul class="unstyled list-box">
				<li>
					<div class="row-fluid">
						<div class="span2">
							<div class="placeholder" style="height: 103px;"></div>
						</div>
						<div class="span8">
							

							<h4><a href="<?=site_url('employees/details/' . $v['id']) . '/' . $job['id'];?>"><?=$v['firstname'] . ' ' . $v['lastname'];?></a> <small>Web Developer | Philippines</small></h4>
							
							<table>
								<tr>
									<td class="text-right">Skills:</td>
									<td><?=$v['skills'];?></td>
								</tr>
								<tr>
									<td class="text-right">Experience:</td>
									<td><?=$v['experience'];?> Year(s)</td>
								</tr>

								<tr>
									<td class="text-right">Bid Description:</td>
									<td><?=$v['bid_desc'];?></td>
								</tr>
							</table>

						</div>

						<div class="span2">
							<div class="">
								<h5>$8.00 / hour</h5>
								<a class="btn btn-success blue" href="<?=site_url('workrooms/create/' . $job['id'] . '/' . $v['id']);?>">Interview</a>
								<a href="<?=site_url('jobs/hire/' . encrypt_id($job['id']) . '/' .encrypt_id($v['id']))?>" class="btn green">Hire</a>
							</div>
						</div>
					</div>

					
				</li>
			</ul>
			<?php } ?>

		</div>
	</div>

	<?php if($this->session->userdata('role') == 'employer'){?>
	<?=$this->load->view('employers/side');?>
	<?php } ?>
</div>

<?=$this->load->view('jobs/apply');?>




