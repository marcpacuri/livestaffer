<?php 

class Workroom_db extends CI_Model{

	function details($hash){

		$this->db->where('hash', $hash);
		$results = $this->db->get('workrooms')->row_array();

		if($results){
			// $results['rate'] = 0;

			if(!empty($results['rate'])){
				$results['rate'] = $results['rate'] / 3600;
			}

			$this->db->where('id', $results['employee_id']);
			$employee = $this->db->get('employees')->row_array();
			if($employee){
				// $results['rate'] = $employee['rate'] / 3600;
			}

		
			$this->db->where('id', $results['employer_id']);
			$employer = $this->db->get('employers')->row_array();

			if($this->session->userdata('role') == 'employer'){
				$results['sender'] = $employer;
				$results['receiver'] = $employee;
			} else{
				$results['receiver'] = $employer;
				$results['sender'] = $employee;
			}

			if($results['receiver']['profile_pic']){
				$results['receiver']['profile_pic'] = base_url('assets/profile_pic/thumb_' . $results['receiver']['profile_pic']);
			} else{
				$results['receiver']['profile_pic'] = base_url('assets/metronic/img//user-small.png');
			}

			$results['employee'] = $employee;
			$results['employer'] = $employer;
		}

		return $results;

	}

	function logs($workroom_id, $limit = NULL){

		if($limit){
			$this->db->limit($limit);
		}

		$this->db->where('workroom_id', $workroom_id);
		$this->db->order_by('created_at', 'desc');
		$results = $this->db->get('work_logs')->result_array();

		return $results; 
	}

	function last_log($workroom_id){
		$this->db->where('workroom_id', $workroom_id);
		$this->db->order_by('created_at', 'desc');
		$this->db->limit(1);
		$results = $this->db->get('work_logs')->row_array();

		if($results){
			$results['hours'] = $this->secondsToTime($results['total_time'], 'hours');
			$results['minutes'] = $this->secondsToTime($results['total_time'], 'minutes');
			$results['seconds'] = $this->secondsToTime($results['total_time'], 'seconds');
			$results['cost'] = number_format($results['cost'], 2, '.', ',');
		} else{
			$results['hours'] = '00';
			$results['minutes'] = '00';
			$results['seconds'] = '00';
			$results['cost'] = '0.00';
			$results['total_time'] = 0;
		}

		return $results;
	}

	// Prefix single-digit values with a zero.
	function ensure2Digit($number) {
	    if($number < 10) {
	        $number = '0' . $number;
	    }
	    return $number;
	}

	// Convert seconds into months, days, hours, minutes, and seconds.
	function secondsToTime($ss, $type) {
	    $s = $this->ensure2Digit($ss%60);
	    $m = $this->ensure2Digit(floor(($ss%3600)/60));
	    $h = $this->ensure2Digit(floor(($ss%86400)/3600));
	    $d = $this->ensure2Digit(floor(($ss%2592000)/86400));
	    $M = $this->ensure2Digit(floor($ss/2592000));

	    $time = '';
	    switch($type){
	    	case 'hours':
	    		$time = $h;
	    		break;
	    	case 'minutes':
	    		$time = $m; 
	    		break;
	    	case 'seconds'; 
	    		$time = $s; 
	    		break;
	    }

	    return $time; 
	}

	function chat_logs($workroom_id, $index = NULL){

		$this->db->limit(5, $index);

		$this->db->order_by('created_at', 'desc');
		$this->db->where('workroom_id', $workroom_id);
		$results = $this->db->get('chat_logs')->result_array();

		foreach($results as $i => $v){
			if(date('m/d/y') == date('m/d/y', strtotime($v['created_at']))){
				$results[$i]['created_at'] = date('h:i A', strtotime($v['created_at']));
			} else{
				$results[$i]['created_at'] = date('M d, h:i A', strtotime($v['created_at']));
			}
			
		}
		return $results; 
	}
}