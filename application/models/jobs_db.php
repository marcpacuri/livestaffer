<?php 

class Jobs_db extends CI_Model{

	function related($user){

		$this->db->where('budget >=', $user['rate']);
		$this->db->where('experience <=', $user['experience']);

		
		$skills = json_decode($user['skills'], TRUE);

		if($skills){
			$string = '(';
			foreach($skills as $i => $v){
				$string .= " skills like '%$v%' OR";
			}

			$string = substr($string, 0, -2) . ')';
			$this->db->where($string);
		}

		$this->db->order_by('created_at', 'DESC');

		$result = $this->db->get('jobs')->result_array();
		return $result;
	}

	function related_employees($job){

		if($job['budget']){
			$this->db->where('rate <=', $job['budget']);
		}
		
		if($job['experience']){
			$this->db->where('experience >=', $job['experience']);
		}
		
		$skills = json_decode($job['skills'], TRUE);

		if($skills){
			$string = '(';
			foreach($skills as $i => $v){
				$string .= " skills like '%$v%' OR";
			}

			$string = substr($string, 0, -2) . ')';
			$this->db->where($string);
		}

		$result = $this->db->get('employees')->result_array();
		return $result;

	}

	function employer_jobs($employer_id, $status){

		$this->db->where('employer_id', $employer_id);
		$this->db->where('status', $status);
		$this->db->order_by('created_at', 'desc');

		$results = $this->db->get('jobs')->result_array();

		return $results; 
	}

	function employer_in_progress($user_id){
		$results = $this->employer_jobs($user_id, 'in_progress');
		foreach($results as $i => $v){

			$results[$i]['workroom'] = NULL;

			$this->db->where('job_id', $v['id']);
			$this->db->where('employee_id', $v['employee_id']);
			$this->db->where('employer_id', $v['employer_id']);
			$room = $this->db->get('workrooms')->row_array();

			if($room){
				$results[$i]['workroom'] = $room['hash'];
			}
			
		}

		return $results; 
	}

	function employer_complete($user_id){
		$results = $this->employer_jobs($user_id, 'complete');
		foreach($results as $i => $v){

			$results[$i]['workroom'] = NULL;

			$this->db->where('job_id', $v['id']);
			$this->db->where('employee_id', $v['employee_id']);
			$this->db->where('employer_id', $v['employer_id']);
			$room = $this->db->get('workrooms')->row_array();

			if($room){
				$results[$i]['workroom'] = $room['hash'];
			}
			
		}

		return $results; 
	}


}

?>