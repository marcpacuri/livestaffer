<?php

class Employees_db extends CI_Model{

	function jobs($employer_id, $status){

		$this->db->where('employee_id', $employer_id);
		$this->db->where('status', $status);
		$this->db->order_by('created_at', 'desc');

		$results = $this->db->get('jobs')->result_array();

		return $results; 
	}

	function job_in_progress($user_id){
		$results = $this->jobs($user_id, 'in_progress');
		foreach($results as $i => $v){

			$results[$i]['workroom'] = NULL;

			$this->db->where('job_id', $v['id']);
			$this->db->where('employee_id', $v['employee_id']);
			$this->db->where('employer_id', $v['employer_id']);
			$room = $this->db->get('workrooms')->row_array();

			if($room){
				$results[$i]['workroom'] = $room['hash'];
			}
			
		}

		return $results; 
	}

	function job_complete($user_id){
		$results = $this->jobs($user_id, 'complete');
		foreach($results as $i => $v){

			$results[$i]['workroom'] = NULL;

			$this->db->where('job_id', $v['id']);
			$this->db->where('employee_id', $v['employee_id']);
			$this->db->where('employer_id', $v['employer_id']);
			$room = $this->db->get('workrooms')->row_array();

			if($room){
				$results[$i]['workroom'] = $room['hash'];
			}
			
		}

		return $results; 
	}
}