<?php

class Notifications_db extends CI_Model{

	function get($role, $user_id){

		$this->db->where('role', $role);

		if($role == 'employee'){
			$this->db->where('employee_id' , $user_id);
		} else{
			$this->db->where('employer_id' , $user_id);
		}

		$this->db->order_by('created_at', 'asc');
		$this->db->limit(10);

		$results = $this->db->get('notifications')->result_array();
		$results = $this->parse($results);

		return $results; 
	}

	function new_items($role, $user_id){

		$this->db->where('role', $role);
		
		if($role == 'employee'){
			$this->db->where('employee_id' , $user_id);
		} else{
			$this->db->where('employer_id' , $user_id);
		}

		$this->db->where('status', 0);
		$this->db->order_by('created_at', 'asc');

		$results = $this->db->get('notifications')->result_array();
		$results = $this->parse($results);


		return $results; 
	}

	function parse($results){

		$array = array();

		foreach($results as $i => $v){

			switch($v['event']){

				case 'new_job': 
					$job = $this->default_db->find('jobs', $v['job_id']);

					if($job){
						$array[] = array(
							'link' => site_url('jobs/details/' . $v['job_id']), 
							'message' => 'Job Listing - ' .  substr($job['title'], 0, 20) . '...', 
							'event' => $v['event'], 
							'status' => $v['status'],
							'time' => $v['created_at']
						);
					}
					
					break;

				case 'new_applicant': 
					$job = $this->default_db->find('jobs', $v['job_id']);
					if($job){
						$array[] = array(
							'link' => site_url('jobs/details/' . $v['job_id']), 
							'message' => 'New Applicant - ' .  substr($job['title'], 0, 20) . '...', 
							'event' => $v['event'], 
							'status' => $v['status'],
							'time' => $v['created_at']
						);
					}
					break;

				case 'intiate_room': 
					$job = $this->default_db->find('jobs', $v['job_id']);
					if($job){
						$array[] = array(
							'link' => site_url('workrooms/room?joinRoom=' . $v['room_id']), 
							'message' => 'Interview Invite - ' .  substr($job['title'], 0, 20) . '...', 
							'event' => $v['event'], 
							'status' => $v['status'],
							'time' => $v['created_at']
						);
					}
					break;

			}
		}

		return $array;

	}
}