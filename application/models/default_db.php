<?php 

class Default_db extends CI_Model{


	function search($database, $post){
		$results = $this->db->get_where($database, $post)->result_array();

		return $results;
	}

	function find($database, $id){
		$results = $this->db->get_where($database, array('id' => $id))->row_array();
		return $results; 
	}

	function find_where($database, $where){
		$results = $this->db->get_where($database, $where)->row_array();
		return $results; 
	}

	function save($database, $post, $id = NULL){

		$post['updated_at'] = date('Y-m-d H:i:s');

		if($id){
			$this->db->where('id', $id);
			$this->db->update($database, $post);
		} else{
			$post['created_at'] = date('Y-m-d H:i:s');
			$this->db->insert($database, $post);
			$id = $this->db->insert_id();
		}

		return $id;
	}

	function delete($database, $id){
		$this->db->where('id', $id);
		$this->db->delete($database);
	}

	function check_role($role){
		if($this->session->userdata('role') != $role){
			redirect('users/login');
		}
	}

	function fields($database){
		$fields = $this->db->list_fields($database);
		$data = array();
		foreach($fields as $i){
			$data[$i] = NULL;
		}
		return $data;
	}

	function get_list($database, $where = NULL, $limit = NULL, $offset = NULL){

		if($where){
			$this->db->where($where);
		}

		if($limit){
			$this->db->limit($limit, $offset);
		}

		$result = $this->db->get($database)->result_array();

		return $result;


	}

	function save_where($database, $post, $where){

		$post['updated_at'] = date('Y-m-d H:i:s');
		$this->db->where($where);
		$this->db->update($database, $post);
	}
}

?>