<?php 

class Files_db extends CI_Model{

	function get_list($get, $limit = NULL, $offset = NULL){

		$this->db->where('workroom_id', $this->session->userdata('workroom_id'));

		if($limit){

			$this->db->limit($limit, $offset);
			$this->db->order_by('created_at', 'desc');
			$results = $this->db->get('files')->result_array();

			foreach($results as $i => $v){
				$results[$i]['created_at'] = date('m/d/Y h:m A', strtotime($v['created_at'])); 
			}

		} else{
			$this->db->from('files');
			$results = $this->db->count_all_results();
		}

		return $results; 
	}
}