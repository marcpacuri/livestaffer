<?php 

class Files extends CI_Controller{

	function __construct(){
		parent::__construct();

        # Load Amazon S3 Library
        $this->load->library('s3');
	}

    # Upload files in S3
	function upload(){
		$post = $_POST;
        if (!$post)
            redirect('404');


        // Define a destination
        $targetFolder = './assets/files/'; // Relative to the root

        //set new filename
        $name = explode('.', $_FILES['Filedata']['name']);
        $ext = $name[count($name) - 1];

        $new_filename = md5($post['workroom_id'] . date('Y-m-d H:i:s')) . '.' . $ext;

        if (!empty($_FILES)) {
            $tempFile = $_FILES['Filedata']['tmp_name'];
            $targetPath = $targetFolder;
            $targetFile = rtrim($targetPath, '/') . '/' . $new_filename;
            $fileParts = pathinfo($_FILES['Filedata']['name']);

            // move_uploaded_file($tempFile, $targetFile);

            $dt['workroom_id'] = $post['workroom_id'];
            $dt['filename'] = $_FILES['Filedata']['name'];
            $dt['hash'] = $new_filename;
            $this->default_db->save('files', $dt);

            # Upload to Amazon S3

            # Check if bucket exists
            $bucket_name = 'workroom-' . $post['workroom_id'];
            $bucket = $this->s3->getBucket($bucket_name);
            if(!$bucket){
                $this->s3->putBucket($bucket_name);
            }

            $this->s3->putObject(file_get_contents($tempFile), $bucket_name, $new_filename);

            unlink($targetPath . $new_filename);
            
            echo 1; die();

            // $json['json'] = $data;
            // $this->load->view('layouts/ajax', $json);
        }
	}

    # Get files from workroom
    function listings(){

        $get = $_GET;
        $this->load->model('files_db');

        $data['sEcho'] = $_GET['sEcho'];
        $data['iTotalRecords'] = $this->files_db->get_list($get);
        $data['iTotalDisplayRecords'] = $this->files_db->get_list($get);
        $data['aaData'] = $this->files_db->get_list($get, $_GET['iDisplayLength'], $_GET['iDisplayStart']);

        $json['json'] = $data;
        $this->load->view('layouts/ajax', $json);
    }

    # Download files from S3
    function download($filename){

        $this->load->helper('download');

        $file = $this->default_db->find_where('files', array('hash' => $filename));
        $s3 = $this->s3->getObject('workroom-' . $file['workroom_id'], $filename);

        $data = $s3->body;
        $name = $file['filename'];

        force_download($name, $data);
    }

}