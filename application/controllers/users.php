<?php 

class Users extends CI_Controller{

	function __construct(){
		parent::__construct();
	}

	function index(){
		if($this->session->userdata('role') == 'employer'){
			redirect('employers/dashboard');
		} else if($this->session->userdata('role') == 'employee'){
			redirect('employees/dashboard');
		} else{
			redirect('users/login');
		}

		
	}

	# User login
	function login(){
		$data['title'] = "Account Login";
		$data['login'] = TRUE;

		$this->form_validation->set_rules('username', 'Username', 'required|callback__auth');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if($this->form_validation->run()){
			//VALIDATION
			$post = $_POST;
			$user = $this->_auth($post, TRUE);
			$this->_set_session($user['id'], $post['role']);

			if($post['role'] == 'employer'){
				redirect('employers/dashboard');
			} else{
				redirect('employees/dashboard');
			}

		} else{
			$data['content'] = $this->load->view('users/login', $data, TRUE);
			$this->load->view('layouts/login', $data);
		}
	}

	# authentication
	function _auth($post = NULL, $return = NULL){
		$post = $_POST;
		$this->form_validation->set_message('_auth', 'Invalid Username or Password');
		$role = $post['role'];
		unset($post['role']);

		if($role == 'employer'){
			$user = $this->default_db->search('employers', $post);
		} else{
			$user = $this->default_db->search('employees', $post);
		}

		if($return){
			if($user){
				return $user[0];
			} else{
				return FALSE;
			}
		} else{
			if(count($user) > 0){
				return TRUE;
			} else{
				return FALSE;
			}
		}
	}

	# Set login session
	function _set_session($id, $role){

		if($role == 'employer'){
			$user = $this->default_db->find('employers', $id);
		} else{
			$user = $this->default_db->find('employees', $id);
		}
		
		$this->session->set_userdata('user_id', $id);
		$this->session->set_userdata('username', $user['username']);
		$this->session->set_userdata('firstname', $user['firstname']);
		$this->session->set_userdata('lastname', $user['lastname']);
		$this->session->set_userdata('role', $role);

		if(isset($user['profile_pic']) && $user['profile_pic']){
			$this->session->set_userdata('profile_pic_thumb', base_url('assets/profile_pic/thumb_' . $user['profile_pic']));
		} else{
			$this->session->set_userdata('profile_pic_thumb', base_url('assets/metronic/img/user-small.png')); 
		}

		if($user['timezone']){
			
			$timezone = timezone_by_offset(timezones($user['timezone']));
			$this->session->set_userdata("timezone", $timezone);

		}
	}

	# Logout
	function logout(){
		$this->session->sess_destroy();
		redirect('users/login');
	}

	# Testing validate paypal account
	function validate(){

		$data['title'] = 'Validate';
		$post = $_POST;

		$this->form_validation->set_rules('email', 'Email', 'required|callback__paypal_validate|valid_email');

		if($this->form_validation->run()){

			$role = $this->session->userdata('role');
			
			if($role == 'employer'){

				$info['paypal_email'] = $post['email'];
				$info['paypal_verified'] = 1;
				$this->default_db->save('employers', $info, $this->session->userdata('user_id'));

				if($this->session->userdata('redirect')){
					$redirect = $this->session->userdata('redirect');
					$this->session->unset_userdata('redirect');
					redirect($redirect);
				} else{
					redirect('employers/dashboard');
				}

			} else{

				$info['paypal_email'] = $post['email'];
				$info['paypal_verified'] = 1;
				$this->default_db->save('employees', $info, $this->session->userdata('user_id'));

				if($this->session->userdata('redirect')){
					$redirect = $this->session->userdata('redirect');
					$this->session->unset_userdata('redirect');
					redirect($redirect);
				} else{
					redirect('employees/dashboard');
				}
			}


		} else{
			$data['post']['email'] = NULL;
			$data['post']['firstName'] = NULL;
			$data['post']['lastName'] = NULL;
			$data['content'] = $this->load->view('users/validate', $data, TRUE);
			$this->load->view('layouts/default', $data);
		}
	
	}

	function _paypal_validate(){

		$this->form_validation->set_message('_paypal_validate', 'Invalid Account');

		$this->config->load('paypal');
		$post = $_POST; 

		$config = array(
			'Sandbox' => $this->config->item('Sandbox'), 			// Sandbox / testing mode option.
			'APIUsername' => $this->config->item('APIUsername'), 	// PayPal API username of the API caller
			'APIPassword' => $this->config->item('APIPassword'), 	// PayPal API password of the API caller
			'APISignature' => $this->config->item('APISignature'), 	// PayPal API signature of the API caller
			'APISubject' => '', 									// PayPal API subject (email address of 3rd party user that has granted API permission for your app)
			'APIVersion' => $this->config->item('APIVersion'), 		// API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
			'DeviceID' => $this->config->item('DeviceID'), 
			'ApplicationID' => $this->config->item('ApplicationID'), 
			'DeveloperEmailAccount' => $this->config->item('DeveloperEmailAccount')
		);

		$this->load->library('paypal/paypal_adaptive', $config);
		$GetVerifiedStatusFields = array(
										'EmailAddress' => $post['email'], //'marcpacuri-facilitator@gmail.com', 					// Required.  The email address of the PayPal account holder.
										'FirstName' => $post['firstName'],// 'Marc', 						// The first name of the PayPal account holder.  Required if MatchCriteria is NAME
										'LastName' => $post['lastName'], //'Pacuri', 						// The last name of the PayPal account holder.  Required if MatchCriteria is NAME
										'MatchCriteria' => 'NAME'					// Required.  The criteria must be matched in addition to EmailAddress.  Currently, only NAME is supported.  Values:  NAME, NONE   To use NONE you have to be granted advanced permissions
										);
		
		$PayPalRequestData = array('GetVerifiedStatusFields' => $GetVerifiedStatusFields);

		$PayPalResult = $this->paypal_adaptive->GetVerifiedStatus($PayPalRequestData);
		

		if($PayPalResult['Ack'] == 'Success'){
			return TRUE;
		} else{
			return FALSE;
		}
	}

	function skip_validation(){
		$role = $this->session->userdata('role');
		if($role == 'employer'){
			redirect('employers/dashboard');
		} else{
			redirect('employees/dashboard');
		}
	}
}

?>