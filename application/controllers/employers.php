<?php 

class Employers extends CI_Controller{

	private $user_id;

	function __construct(){
		parent::__construct();

		# Check if logged in 
		$allow = array('profile_pic', 'register');

		if(!in_array($this->uri->segment(2), $allow)){
			$this->default_db->check_role('employer');
			$this->user_id = $this->session->userdata('user_id');
		}
	}

	# Employers dashboard
	function dashboard(){
		$data['title'] = 'Dashboard';
		$where['status'] = 'open';
		
		$data['open_jobs'] = $this->jobs_db->employer_jobs($this->user_id, 'open');
		$data['in_progress'] = $this->jobs_db->employer_in_progress($this->user_id);
		$data['complete'] = $this->jobs_db->employer_complete($this->user_id);

		$data['content'] = $this->load->view('employers/dashboard', $data, TRUE);
		$this->load->view('layouts/default', $data);
	}

	# Edit account details
	function account(){
		$data['title'] = 'Account';
		
		$this->form_validation->set_rules('firstname', 'First Name', 'required');

		if($this->form_validation->run()){
			$post = $_POST;
			$this->default_db->save('employers', $post, $this->user_id);
			$this->session->set_flashdata('notify', 'Record Updated');
			redirect('employers/account');
		} else{
			$data['post'] = $this->default_db->find('employers', $this->user_id);
		}

		$data['js'][] = base_url('assets/uploadify/uploadify.js');
		$data['js'][] = base_url('assets/js/employer_pic.js');
		$data['css'][] = base_url('assets/uploadify/uploadify.css');

		$data['content'] = $this->load->view('employers/account', $data, TRUE);
		$this->load->view('layouts/default', $data);
	}

	# Change password
	function password(){
		$data['title'] = 'Account';
		
		$this->form_validation->set_rules('password', 'Password', 'required|matches[password_conf]|callback__old_password');
		$this->form_validation->set_rules('password_conf', 'Confirm Password', 'required');

		if($this->form_validation->run()){
			$post = $_POST;
			unset($post['password_conf']);
			unset($post['old_password']);
			$this->default_db->save('employers', $post, $this->user_id);
			$this->session->set_flashdata('notify', 'Record Updated');
			redirect('employers/password');
		} else{
			$data['post'] = $this->default_db->find('employers', $this->user_id);
		}

		$data['content'] = $this->load->view('employers/password', $data, TRUE);
		$this->load->view('layouts/default', $data);
	}

	# Validate old password
	function _old_password(){
		$post = $_POST;
		$this->form_validation->set_message('_old_password', 'Invalid Password');
		$user = $this->default_db->find('employers', $this->user_id);

		if($user['password'] == $post['old_password']){
			return TRUE;
		} else{
			return FALSE;
		}
	}

	# Browser notification permisions
	function notification_settings(){
		$data['title'] = 'Notification Settings';
		$data['content'] = $this->load->view('employees/notification_settings', $data, TRUE);
		$this->load->view('layouts/default', $data);
	}

	# Get notification as json
	function notifications(){
		$data['notifications'] = $this->notifications_db->get('employer', $this->user_id);
		$json['json'] = $data;
		$this->load->view('layouts/ajax', $json);
	}

	# Get new notifications
	function new_notifications(){
		$data['notifications'] = $this->notifications_db->new_items('employer', $this->user_id);
		$json['json'] = $data;
		$this->load->view('layouts/ajax', $json);
	}

	# Tag notifiaction as read
	function notifications_read(){
		$dt['status'] = 1;
		$where['employee_id'] = $this->user_id;
		$where['role'] = 'employer';
		$this->default_db->save_where('notifications', $dt, $where);
	}

	# Change profile pic
	function profile_pic(){

		$post = $_POST; 
		if(!$post) redirect('404');

		$post = $_POST;
        if (!$post)
            redirect('404');

        // Define a destination
        $targetFolder = './assets/profile_pic/'; // Relative to the root

        //set new filename
        $name = explode('.', $_FILES['Filedata']['name']);
        $ext = $name[count($name) - 1];

        $new_filename = md5(rand() . date('Y-m-d H:i:s')) . '.' . $ext;

        if (!empty($_FILES)) {
            $tempFile = $_FILES['Filedata']['tmp_name'];
            $targetPath = $targetFolder;
            $targetFile = rtrim($targetPath, '/') . '/' . $new_filename;
            $fileParts = pathinfo($_FILES['Filedata']['name']);

            # Save a temporary file to the server
            move_uploaded_file($tempFile, $targetFile);

            # Save filename in user details
            $dt['profile_pic'] = $new_filename;
            $this->default_db->save('employers', $dt, $post['user_id']);
            $this->session->set_userdata('profile_pic_thumb', base_url('assets/profile_pic/' . $new_filename));

            # Make 3 sizes
            $this->load->library('photos');
            $this->photos->process($targetFolder, $new_filename);

            $data['result'] = 1;

            $json['json'] = $data;
            $this->load->view('layouts/ajax', $json);
        }
	}

	# Delete profile pic
	function delete_picture(){

		$employee = $this->default_db->find('employers', $this->session->userdata('user_id'));
		$info['profile_pic'] = NULL; 
		$this->default_db->save('employers', $info, $this->session->userdata('user_id'));
		$pic = $this->session->userdata('profile_pic_thumb');

		if(file_exists('./assets/profile_pic/' . $employee['profile_pic'])) 
			unlink('./assets/profile_pic/' . $employee['profile_pic']);
		if(file_exists('./assets/profile_pic/thumb_' . $employee['profile_pic'])) 
			unlink('./assets/profile_pic/thumb_' . $employee['profile_pic']);

		$this->session->set_userdata('profile_pic_thumb', base_url('assets/metronic/img/user-small.png')); 
		redirect($this->agent->referrer());
	}

	# Employers registration
	function register(){
		$data['title'] = 'Register';
		$post = $_POST;

		if(!$post){
			$post = $this->default_db->fields('employers');
			$post['paypal_email'] = NULL;
			$post['firstName'] = NULL;
			$post['lastName'] = NULL;
		}
		
		$post['role'] = 'employer';

		
		$this->form_validation->set_rules('username', 'Username', 'required|is_unique[employers.username]');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[employers.email]|callback__paypal_validate');
	
		
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('password_conf', 'Password Confirmation', 'required|matches[password]');

		if($this->form_validation->run()){
	
			$role = $post['role'];
			unset($post['role']);
			unset($post['password_conf']);
			unset($post['paypal_email']);
			unset($post['firstName']);
			unset($post['lastName']);
			$this->session->set_flashdata('Welcome ' . $post['firstname']);

			$post['paypal_verified'] = 1;
			$id = $this->default_db->save('employers', $post);
			$this->_set_session($id, $role);

			redirect('employers/dashboard');
		
		} else{
			$data['post'] = $post;
			$data['content'] = $this->load->view('employers/register', $data, TRUE);
		}

		$this->load->view('layouts/blank', $data);
	}

	# Validate if paypal account is verified
	function _paypal_validate(){

		$this->form_validation->set_message('_paypal_validate', 'Your paypal account is not valid');

		$this->config->load('paypal');
		$post = $_POST; 

		$config = array(
			'Sandbox' => $this->config->item('Sandbox'), 			// Sandbox / testing mode option.
			'APIUsername' => $this->config->item('APIUsername'), 	// PayPal API username of the API caller
			'APIPassword' => $this->config->item('APIPassword'), 	// PayPal API password of the API caller
			'APISignature' => $this->config->item('APISignature'), 	// PayPal API signature of the API caller
			'APISubject' => '', 									// PayPal API subject (email address of 3rd party user that has granted API permission for your app)
			'APIVersion' => $this->config->item('APIVersion'), 		// API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
			'DeviceID' => $this->config->item('DeviceID'), 
			'ApplicationID' => $this->config->item('ApplicationID'), 
			'DeveloperEmailAccount' => $this->config->item('DeveloperEmailAccount')
		);

		$this->load->library('paypal/paypal_adaptive', $config);
		$GetVerifiedStatusFields = array(
										'EmailAddress' => $post['paypal_email'], //'marcpacuri-facilitator@gmail.com', 					// Required.  The email address of the PayPal account holder.
										'FirstName' => $post['firstName'],// 'Marc', 						// The first name of the PayPal account holder.  Required if MatchCriteria is NAME
										'LastName' => $post['lastName'], //'Pacuri', 						// The last name of the PayPal account holder.  Required if MatchCriteria is NAME
										'MatchCriteria' => 'NAME'					// Required.  The criteria must be matched in addition to EmailAddress.  Currently, only NAME is supported.  Values:  NAME, NONE   To use NONE you have to be granted advanced permissions
										);
		
		$PayPalRequestData = array('GetVerifiedStatusFields' => $GetVerifiedStatusFields);

		$PayPalResult = $this->paypal_adaptive->GetVerifiedStatus($PayPalRequestData);
		

		if($PayPalResult['Ack'] == 'Success'){
			return TRUE;
		} else{
			return FALSE;
		}
	}

	# Set employers details in session
	function _set_session($id, $role){

	
		$user = $this->default_db->find('employers', $id);

		$this->session->set_userdata('user_id', $id);
		$this->session->set_userdata('username', $user['username']);
		$this->session->set_userdata('firstname', $user['firstname']);
		$this->session->set_userdata('lastname', $user['lastname']);
		$this->session->set_userdata('role', $role);

		if(isset($user['profile_pic']) && $user['profile_pic']){
			$this->session->set_userdata('profile_pic_thumb', base_url('assets/profile_pic/thumb_' . $user['profile_pic']));
		} else{
			$this->session->set_userdata('profile_pic_thumb', base_url('assets/metronic/img/user-small.png')); 
		}

		if($user['timezone']){
			
			$timezone = timezone_by_offset(timezones($user['timezone']));
			$this->session->set_userdata("timezone", $timezone);

		}
	}
}

?>