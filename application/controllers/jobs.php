<?php 

class Jobs extends CI_Controller{

	private $user_id;

	function __construct(){
		parent::__construct();
		$this->user_id = $this->session->userdata('user_id');
	}

	# Add and Edit Job
	function save($id = NULL){

		$this->default_db->check_role('employer');

		$data['title'] = "Job";
		$data['skills'] = $this->dataset_db->skills();
		$data['id'] = $id;

		if($id){
			$data['job'] = $this->default_db->find('jobs', $id);
			$data['job']['skills'] = json_decode($data['job']['skills'], TRUE); 

			if(!$data['job']['skills']){
				$data['job']['skills'] = array();
			}

		} else{
			$data['job'] = $this->default_db->fields('jobs');
			$data['job']['skills'] = array();
		}

		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('skills[]', 'Skills', 'required');

		if($this->form_validation->run()){
			$post = $_POST;
			$post['employer_id'] = $this->user_id; 
			$post['skills'] = json_encode($post['skills']);
			if(!$id){
				$post['status'] = 'open';
				$this->session->set_flashdata('new_job', 1);
			} 
			
			$id = $this->default_db->save('jobs', $post, $id);
			$this->session->set_flashdata('notice', 'Record Saved');

			$this->_notifications($id);

			redirect('jobs/details/' . $id);
		} else{

			$data['content'] = $this->load->view('jobs/save', $data, TRUE);
			$this->load->view('layouts/default', $data);
		}

	}

	# View job details
	function details($id){
		$data['title'] = "Job";
		$data['job'] = $this->default_db->find('jobs', $id);
		$data['job']['skills'] = json_decode($data['job']['skills'], TRUE);
		$data['applicants']  = array();

		$where_jobs['employee_id'] = $this->user_id;
		$where_jobs['job_id'] = $id;
		$apply = $this->default_db->search('applicants', $where_jobs);
		$applicants = array();

		foreach($apply as $i => $v){
			$applicants[$i] = $this->default_db->find('employees', $v['employee_id']);
			$applicants[$i]['workroom'] = md5($v['job_id'] . $v['employee_id']);
			$applicants[$i]['bid_desc'] = $v['bid_desc'];
		}

		$data['applicants'] = $applicants;
		
		if($this->session->userdata('role') == 'employee'){
			$where['job_id'] = $id;
			$where['employee_id'] = $this->user_id;
			$data['apply'] = $this->default_db->search('applicants', $where);
		}

		$data['js'][] = base_url('assets/js/jobs.js');
	
		$data['content'] = $this->load->view('jobs/show', $data, TRUE);
		$this->load->view('layouts/default', $data);
	}

	# Apply for the job
	function apply(){

		$post = $_POST;
		if(!$post) redirect('404');

		$this->default_db->check_role('employee');
		$data['job_id'] = $post['id'];
		$data['employee_id'] = $this->user_id;

		$job = $this->default_db->search('applicants', $data); 

		if(!$job){
			$data['bid_desc'] = $post['bid_desc'];
			$this->session->set_flashdata('notify', 'Application Sent');
			$this->default_db->save('applicants', $data);
			$this->_notify_apply($data['job_id']);

		}

		$this->session->set_flashdata('apply_broadcast', 1);
		
		redirect('jobs/details/' . $post['id']);
	}

	# Withdraw application
	function withdraw($id){

		$this->default_db->check_role('employee');
		
		$this->default_db->check_role('employee');
		$data['job_id'] = $id;
		$data['employee_id'] = $this->user_id;

		$job = $this->default_db->search('applicants', $data); 

		if($job){
			$this->session->set_flashdata('notify', 'Application Withdrawn');
			$this->default_db->delete('applicants', $job[0]['id']);
		}
		
		redirect($this->agent->referrer());
	}

	
	# Notificaiton for application
	function _notify_apply($job_id){

		$job = $this->default_db->find('jobs', $job_id);

		$info['job_id'] = $job['id']; 
		$info['employee_id'] = $this->user_id;
		$info['employer_id'] = $job['employer_id'];
		$info['event'] = 'new_applicant';
		$info['role'] = 'employer';
		$this->default_db->save('notifications', $info);
	}

	function _notifications($id){
		$this->load->model('jobs_db');

		$job = $this->default_db->find('jobs', $id);
		$employees = $this->jobs_db->related_employees($job);

		foreach($employees as $i => $v){

			$where['job_id'] = $id; 
			$where['employee_id'] = $v['id'];

			$item = $this->default_db->search('notifications', $where);

			if(!$item){
				$info['job_id'] = $id; 
				$info['employee_id'] = $v['id'];
				$info['employer_id'] = $this->user_id;
				$info['event'] = 'new_job';
				$info['role'] = 'employee';
				$this->default_db->save('notifications', $info);
			} 
		}

	}

	# Create workroom
	function initiate_room(){
		$post = $_POST; 
	
		$info['job_id'] = $post['job_id']; 
		$info['employee_id'] = $post['employee_id'];
		$info['employer_id'] = $this->user_id;
		$info['room_id'] = $post['room_id'];
		$info['event'] = 'intiate_room';
		$info['role'] = 'employee';
		$this->default_db->save('notifications', $info);

		$data['post'] = $post;
		$json['json'] = $data; 
		$this->load->view('layouts/ajax', $json);
	}

	# Hire the employee
	function hire($job_id, $employee_id){

		$job_id = decrypt_id($job_id);
		$employee_id = decrypt_id($employee_id);
		if(!$employee_id || !$job_id) redirect('404');

		$info['employee_id'] = $employee_id;
		$info['status'] = 'in_progress';
		$this->default_db->save('jobs', $info, $job_id);

		//CREATE WORKROOM
		redirect('workrooms/create/' . encrypt_id($job_id) . '/' . encrypt_id($employee_id));

	}
}

?>