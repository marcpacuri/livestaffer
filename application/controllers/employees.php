<?php 

class Employees extends CI_Controller{

	private $user_id;

	function __construct(){
		parent::__construct();

		$allow = array('details', 'register');

		if(!in_array($this->uri->segment(2), $allow)){
			$this->default_db->check_role('employee');
		}


		$this->user_id = $this->session->userdata('user_id');
		$this->load->model('employees_db');
		$this->load->library('s3');

		# Set timezone
		if($this->session->userdata('timezone')){
			date_default_timezone_set($this->session->userdata('timezone'));
		}
		
	}

	# This is a view of the emplyoees dashboard
	function dashboard(){

		$data['title'] = 'Dashboard';
		$this->load->model('jobs_db');
		$user = $this->default_db->find('employees', $this->user_id);

		if(empty($user['skills']) || empty($user['rate']) || empty($user['experience']) ){
			$data['open_jobs'] = NULL;
		} else{
			$data['open_jobs'] = $this->jobs_db->related($user);
		}

		$data['in_progress'] = $this->employees_db->job_in_progress($this->user_id);
		$data['complete'] = $this->employees_db->job_complete($this->user_id);
		
		$data['content'] = $this->load->view('employees/dashboard', $data, TRUE);
		$this->load->view('layouts/default', $data);
	}

	# This is used to edit the employees account details
	function account(){
		$data['title'] = 'Account';
		
		$this->form_validation->set_rules('firstname', 'First Name', 'required');

		if($this->form_validation->run()){
			$post = $_POST;
			$this->default_db->save('employees', $post, $this->user_id);
			$this->session->set_flashdata('notify', 'Record Updated');
			redirect('employees/account');
		} else{
			$data['post'] = $this->default_db->find('employees', $this->user_id);
		}


		$data['js'][] = base_url('assets/uploadify/uploadify.js');
		$data['js'][] = base_url('assets/js/employee_pic.js');
		$data['css'][] = base_url('assets/uploadify/uploadify.css');

		$data['content'] = $this->load->view('employees/account', $data, TRUE);
		$this->load->view('layouts/default', $data);
	}

	# For changing password
	function password(){
		$data['title'] = 'Account';
		
		$this->form_validation->set_rules('password', 'Password', 'required|matches[password_conf]|callback__old_password');
		$this->form_validation->set_rules('password_conf', 'Confirm Password', 'required');

		if($this->form_validation->run()){
			$post = $_POST;
			unset($post['password_conf']);
			unset($post['old_password']);
			$this->default_db->save('employees', $post, $this->user_id);
			$this->session->set_flashdata('notify', 'Record Updated');
			redirect('employees/password');
		} else{
			$data['post'] = $this->default_db->find('employees', $this->user_id);
		}

		$data['content'] = $this->load->view('employees/password', $data, TRUE);
		$this->load->view('layouts/default', $data);
	}

	# Old password validation
	function _old_password(){
		$post = $_POST;
		$this->form_validation->set_message('_old_password', 'Invalid Password');
		$user = $this->default_db->find('employees', $this->user_id);

		if($user['password'] == $post['old_password']){
			return TRUE;
		} else{
			return FALSE;
		}
	}

	# Used to edit the employees skills
	function skills(){
		$data['title'] = 'Skills';
		$data['skills'] = $this->dataset_db->skills();
		$this->form_validation->set_rules('skills', 'Skills', 'required');

		if($this->form_validation->run()){
			$post = $_POST;

			if(!isset($post['skills'])){
				$post['skills'] = NULL;
			} else{
				$post['skills'] = json_encode($post['skills']);
			}

			$this->default_db->save('employees', $post, $this->user_id);
			redirect('employees/skills');
		} else{
			$user = $this->default_db->find('employees', $this->user_id);
			$data['user'] = $user;
			$data['user']['skills'] = empty($user['skills']) ? array() : json_decode($user['skills'], TRUE);
			$data['content'] = $this->load->view('employees/skills', $data, TRUE);
			$this->load->view('layouts/default', $data);
		}
	}


	function details($id, $job_id = NULL){

		$this->default_db->check_role('employer');

		$user = $this->default_db->find('employees', $id);

		if($user){
			$data['user'] = $user;
			$data['job_id'] = $job_id;
			$data['workroom'] = md5($job_id . $id);
			$data['title'] = $user['firstname'] . ' ' . $user['lastname'];
		} else{
			show_404();
		}
		
		$data['content'] = $this->load->view('employees/details', $data, TRUE);
		$this->load->view('layouts/default', $data);
	}

	# For setting the browser notification permission
	function notification_settings(){
		$data['title'] = 'Notification Settings';
		$data['content'] = $this->load->view('employees/notification_settings', $data, TRUE);
		$this->load->view('layouts/default', $data);
	}

	# Used to grab notifications and prints it out as json format
	function notifications(){
		$data['notifications'] = $this->notifications_db->get('employee', $this->user_id);
		$json['json'] = $data;
		$this->load->view('layouts/ajax', $json);
	}

	# Used to grab undread notifications and prints it out as json format
	function new_notifications(){
		$data['notifications'] = $this->notifications_db->new_items('employee', $this->user_id);
		$json['json'] = $data;
		$this->load->view('layouts/ajax', $json);
	}

	# Tags the notification as read
	function notifications_read(){
		$dt['status'] = 1;
		$where['employee_id'] = $this->user_id;
		$where['role'] = 'employee';
		$this->default_db->save_where('notifications', $dt, $where);
	}

	# Changing profile pic
	function profile_pic(){

		$post = $_POST; 
		if(!$post) redirect('404');

		$post = $_POST;
        if (!$post)
            redirect('404');

        // Define a destination
        $targetFolder = './assets/profile_pic/'; // Relative to the root

        //set new filename
        $name = explode('.', $_FILES['Filedata']['name']);
        $ext = $name[count($name) - 1];

        $new_filename = md5(rand() . date('Y-m-d H:i:s')) . '.' . $ext;

        if (!empty($_FILES)) {
            $tempFile = $_FILES['Filedata']['tmp_name'];
            $targetPath = $targetFolder;
            $targetFile = rtrim($targetPath, '/') . '/' . $new_filename;
            $fileParts = pathinfo($_FILES['Filedata']['name']);

            # Save a temporary file to the server
            move_uploaded_file($tempFile, $targetFile);

            # Save filename in user details
            $dt['profile_pic'] = $new_filename;
            $this->default_db->save('employees', $dt, $post['user_id']);
            $this->session->set_userdata('profile_pic_thumb', base_url('assets/profile_pic/' . $new_filename));

            # Make 3 sizes
            $this->load->library('photos');
            $this->photos->process($targetFolder, $new_filename);

            $data['result'] = 1;

            $json['json'] = $data;
            $this->load->view('layouts/ajax', $json);
        }
	}

	# Deleting profile pic
	function delete_picture(){

		$employee = $this->default_db->find('employees', $this->session->userdata('user_id'));
		$info['profile_pic'] = NULL; 
		$this->default_db->save('employees', $info, $this->session->userdata('user_id'));
		$pic = $this->session->userdata('profile_pic_thumb');

		if(file_exists('./assets/profile_pic/' . $employee['profile_pic'])) 
			unlink('./assets/profile_pic/' . $employee['profile_pic']);
		if(file_exists('./assets/profile_pic/thumb_' . $employee['profile_pic'])) 
			unlink('./assets/profile_pic/thumb_' . $employee['profile_pic']);

		$this->session->set_userdata('profile_pic_thumb', base_url('assets/metronic/img/user-small.png')); 
		redirect($this->agent->referrer());
	}

	# For employees registration
	function register(){
		$data['title'] = 'Register';
		$post = $_POST;

		if(!$post){
			$post = $this->default_db->fields('employers');
			$post['paypal_email'] = NULL;
			$post['firstName'] = NULL;
			$post['lastName'] = NULL;
		}

		$post['role'] = 'employee';

		$this->form_validation->set_rules('username', 'Username', 'required|is_unique[employees.username]');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[employees.email]');
	
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('password_conf', 'Password Confirmation', 'required|matches[password]');

		if($this->form_validation->run()){
	
			$role = $post['role'];
			unset($post['role']);
			unset($post['password_conf']);
			unset($post['paypal_email']);
			unset($post['firstName']);
			unset($post['lastName']);
			$this->session->set_flashdata('Welcome ' . $post['firstname']);

			$post['paypal_verified'] = 1;
			$id = $this->default_db->save('employees', $post);
			$this->_set_session($id, $role);

			redirect('employees/dashboard');
		
		} else{
			$data['post'] = $post;
			$data['content'] = $this->load->view('employees/register', $data, TRUE);
		}

		$this->load->view('layouts/blank', $data);
	}

	# Set the employee details in the session
	function _set_session($id, $role){

	
		$user = $this->default_db->find('employees', $id);

		$this->session->set_userdata('user_id', $id);
		$this->session->set_userdata('username', $user['username']);
		$this->session->set_userdata('firstname', $user['firstname']);
		$this->session->set_userdata('lastname', $user['lastname']);
		$this->session->set_userdata('role', $role);

		if(isset($user['profile_pic']) && $user['profile_pic']){
			$this->session->set_userdata('profile_pic_thumb', base_url('assets/profile_pic/thumb_' . $user['profile_pic']));
		} else{
			$this->session->set_userdata('profile_pic_thumb', base_url('assets/metronic/img/user-small.png')); 
		}

		if($user['timezone']){
			
			$timezone = timezone_by_offset(timezones($user['timezone']));
			$this->session->set_userdata("timezone", $timezone);

		}
	}
}
?>