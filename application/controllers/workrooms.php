<?php 


class Workrooms extends CI_Controller{

	private $user_id; 

	function __construct(){
		parent::__construct();

		$this->user_id = $this->session->userdata('user_id');
		if(!$this->user_id){
			redirect('users/login');
		}

		$this->load->model('workroom_db');

		# Set timezone
		date_default_timezone_set($this->session->userdata('timezone'));
	}

	# Create workroom
	function create($job_id = NULL, $employee_id = NULL){

		$job_id = decrypt_id($job_id);
		$employee_id = decrypt_id($employee_id);
		if(!$job_id || !$employee_id) redirect('404');

		$job = $this->default_db->find('jobs', $job_id);

		if(!$job) redirect('404');

		$employee = $this->default_db->find('employees', $employee_id);
		if(!$employee) redirect('404');

		$info['employee_id'] = $employee_id; 
		$info['job_id'] = $job['id'];
		$info['employer_id'] = $job['employer_id'];
		$info['hash'] = md5($info['employee_id'] . $info['job_id'] . date('Y-m-d H:i:s'));
		$info['rate'] = $employee['rate'];
		$this->default_db->save('workrooms', $info);

		$dt['job_id'] = $info['job_id']; 
		$dt['employee_id'] = $info['employee_id'];
		$dt['employer_id'] = $info['employer_id'];
		$dt['room_id'] = $info['hash'];
		$dt['event'] = 'intiate_room';
		$dt['role'] = 'employee';
		$this->default_db->save('notifications', $dt);

		$this->session->set_flashdata('new_room', TRUE);
		redirect('workrooms/room?openRoom=' . $info['hash']);
	}

	# Enter workroom
	function room(){
		$post = $_GET;

		if(isset($post['openRoom']) || isset($post['joinRoom'])){
			$hash = isset($post['openRoom']) ? $post['openRoom'] : $post['joinRoom'];
		} else{
			redirect('404');
		}

		$workroom = $this->workroom_db->details($hash);
		if(!$workroom) redirect('404');

		$data['title'] = 'Workroom';
		$data['workroom'] = $workroom;
		$this->session->set_userdata('workroom_id', $workroom['id']);
		$data['log'] = $this->workroom_db->last_log($workroom['id']);
		
		// $data['js'][] = "https://www.webrtc-experiment.com/RTCMultiConnection-v1.4.js";
		// $data['js'][] = base_url('assets/js/video.js');

		$data['js'][] = base_url('assets/js/chat.js');
		$data['js'][] = base_url('assets/js/jquery.timer.js');
		$data['js'][] = base_url('assets/js/timer.js');
		$data['js'][] = base_url('assets/js/files.js');

		# Simplel WebRTC
		$data['js'][] = base_url('assets/js/simple_webrtc.js');
		$data['js'][] = "http://simplewebrtc.com/latest.js";
		
		$data['js'][] = base_url('assets/uploadify/uploadify.js');
		$data['css'][] = base_url('assets/uploadify/uploadify.css');

		

		$data['content'] = $this->load->view('workrooms/view', $data, TRUE);
		$this->load->view('layouts/default', $data);
	}

	# Notify emplyee for new workroom
	function notify_employee(){
		$post = $_POST; 
	
		$info['job_id'] = $post['job_id']; 
		$info['employee_id'] = $post['employee_id'];
		$info['employer_id'] = $post['employer_id'];
		$info['room_id'] = $post['room_id'];
		$info['event'] = 'intiate_room';
		$info['role'] = 'employee';
		$this->default_db->save('notifications', $info);

		$data['post'] = $post;
		$json['json'] = $data; 
		$this->load->view('layouts/ajax', $json);
	}

	# Log time and cost
	function log(){
		$post = $_POST; 
		if(!$post) redirect('404');

		$workroom = $this->workroom_db->details($post['hash']);

		if($workroom){

			$employee = $this->default_db->find('employees', $workroom['employee_id']);

			$info['total_time'] = $post['total_time'];
			$info['workroom_id'] = $workroom['id'];
			$info['employee_id'] = $workroom['employee_id'];
			$info['employer_id'] = $workroom['employer_id'];
			$info['rate'] = $employee['rate'];
			$info['cost'] = $post['cost'];
			$this->default_db->save('work_logs', $info);
		}

		$data['result'] = TRUE;
		$json['json'] = $data; 
		$this->load->view('layouts/ajax', $json);
	}

	# Log conversation
	function chat_log(){
		$post = $_POST; 
		if(!$post) redirect('404');

		$info['user_id'] = $this->user_id; 
		$info['role'] = $this->session->userdata('role');
		$info['workroom_id'] = $post['workroom_id'];
		$info['message'] = $post['message'];

		$this->default_db->save('chat_logs', $info);

		$data['result'] = TRUE;
		$json['json'] = $data; 
		$this->load->view('layouts/ajax', $json);

	}

	# Fetch previous text chat
	function chat_logs(){
		$post = $_POST; 
		if(!$post) redirect('404');

		$data['messages'] = $this->workroom_db->chat_logs($post['workroom_id'], $post['index']);
		$data['chat_index'] = $post['index'] + 5;
		$json['json'] = $data; 
		$this->load->view('layouts/ajax', $json);
	}

	# Close workroom and pay employee
	function complete($workroom_id){

		if($this->session->userdata('role') != 'employer') redirect('404');

		$workroom_id = decrypt_id($workroom_id); 
		$workroom = $this->default_db->find('workrooms', $workroom_id);
		if(!$workroom) redirect('404');

		if($this->session->userdata('role') == 'employer'){
			$user = $this->default_db->find('employers', $this->session->userdata('user_id'));
		} else{
			$user = $this->default_db->find('employees', $this->session->userdata('user_id'));
		}

		if($user && $user['paypal_verified']){
			

			# Check if employee is verified
			$employee = $this->default_db->find('employees', $workroom['employee_id']);
			if($employee['paypal_verified']){

				# Paypal Chaining

				$this->_chain_payment($workroom);


			} else{
				$data['title'] = "Payment";
				$data['workroom'] = $workroom;
				$data['content'] = $this->load->view('workrooms/payment_error', $data, TRUE);
				$this->load->view('layouts/default', $data);
			}

		} else{
			$this->session->set_userdata('redirect', 'workrooms/complete/' . encrypt_id($workroom_id));
			redirect('users/validate');
		}


	}

	function _chain_payment($workroom){

		$this->config->load('paypal');


		$config = array(
			'Sandbox' => $this->config->item('Sandbox'), 			// Sandbox / testing mode option.
			'APIUsername' => $this->config->item('APIUsername'), 	// PayPal API username of the API caller
			'APIPassword' => $this->config->item('APIPassword'), 	// PayPal API password of the API caller
			'APISignature' => $this->config->item('APISignature'), 	// PayPal API signature of the API caller
			'APISubject' => '', 									// PayPal API subject (email address of 3rd party user that has granted API permission for your app)
			'APIVersion' => $this->config->item('APIVersion'), 		// API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
			'DeviceID' => $this->config->item('DeviceID'), 
			'ApplicationID' => $this->config->item('ApplicationID'), 
			'DeveloperEmailAccount' => $this->config->item('DeveloperEmailAccount')
		);

		$this->load->library('paypal/paypal_adaptive', $config);

		$commission = 0;
		$price = 0;

		# Get employer & employee details
		$employer = $this->default_db->find('employers', $workroom['employer_id']);
		$employee = $this->default_db->find('employees', $workroom['employee_id']);

		# Calucate commision (10%)

		$time_log = $this->workroom_db->last_log($workroom['id']);
		$employee_payment = $time_log['cost'] * 0.90; 
		$employee_payment = number_format($employee_payment, 2, '.', ',');
		// $price = $time_log['cost'] - $commission;
		

		# Save payment request
		$pay['hash'] = md5($workroom['id'] . date('Y-m-d H:i:s'));
		$pay['workroom_id'] = $workroom['id'];
		$pay['employer_id'] = $workroom['employer_id'];
		$pay['employee_id'] = $workroom['employee_id'];
		$pay['employer_email'] = $employer['paypal_email'];
		$pay['employee_email'] = $employee['paypal_email'];
	
		$invoice_id = $this->default_db->save('payment_requests', $pay);

		if(ENVIRONMENT == 'development'){
			$invoice_id = 'DEV-' . $invoice_id;
		}

		// Prepare request arrays
		$PayRequestFields = array(
								'ActionType' => 'PAY', 								// Required.  Whether the request pays the receiver or whether the request is set up to create a payment request, but not fulfill the payment until the ExecutePayment is called.  Values are:  PAY, CREATE, PAY_PRIMARY
								'CancelURL' => site_url('workrooms/room?openRoom=' . $workroom['hash']), 									// Required.  The URL to which the sender's browser is redirected if the sender cancels the approval for the payment after logging in to paypal.com.  1024 char max.
								'CurrencyCode' => 'USD', 								// Required.  3 character currency code.
								'FeesPayer' => 'EACHRECEIVER', 									// The payer of the fees.  Values are:  SENDER, PRIMARYRECEIVER, EACHRECEIVER, SECONDARYONLY
								'IPNNotificationURL' => '', 						// The URL to which you want all IPN messages for this payment to be sent.  1024 char max.
								'Memo' => '', 										// A note associated with the payment (text, not HTML).  1000 char max
								'Pin' => '', 										// The sener's personal id number, which was specified when the sender signed up for the preapproval
								'PreapprovalKey' => '', 							// The key associated with a preapproval for this payment.  The preapproval is required if this is a preapproved payment.  
								'ReturnURL' => site_url('payments/success/' . $pay['hash']), 									// Required.  The URL to which the sener's browser is redirected after approvaing a payment on paypal.com.  1024 char max.
								'ReverseAllParallelPaymentsOnError' => '', 			// Whether to reverse paralel payments if an error occurs with a payment.  Values are:  TRUE, FALSE
								'SenderEmail' => '', 								// Sender's email address.  127 char max.
								'TrackingID' => ''									// Unique ID that you specify to track the payment.  127 char max.
								);
								
		$ClientDetailsFields = array(
								'CustomerID' => '', 								// Your ID for the sender  127 char max.
								'CustomerType' => '', 								// Your ID of the type of customer.  127 char max.
								'GeoLocation' => '', 								// Sender's geographic location
								'Model' => '', 										// A sub-identification of the application.  127 char max.
								'PartnerName' => ''									// Your organization's name or ID
								);
								
		$FundingTypes = array('ECHECK', 'BALANCE', 'CREDITCARD');
		

		$Receivers = array();

		# LiveStaffer
		$Receiver = array(
						'Amount' => $time_log['cost'], 											// Required.  Amount to be paid to the receiver.
						'Email' => 'marcpacuri-facilitator@gmail.com', 												// Receiver's email address. 127 char max.
						'InvoiceID' => $invoice_id, 											// The invoice number for the payment.  127 char max.
						'PaymentType' => 'SERVICE', 										// Transaction type.  Values are:  GOODS, SERVICE, PERSONAL, CASHADVANCE, DIGITALGOODS
						'PaymentSubType' => '', 									// The transaction subtype for the payment.
						'Phone' => array('CountryCode' => '', 'PhoneNumber' => '', 'Extension' => ''), // Receiver's phone number.   Numbers only.
						'Primary' => 'true'												// Whether this receiver is the primary receiver.  Values are boolean:  TRUE, FALSE
						);
		array_push($Receivers,$Receiver);
		
		# Employee
		$Receiver = array(
						'Amount' => $employee_payment, 											// Required.  Amount to be paid to the receiver.
						'Email' => $employee['paypal_email'], 												// Receiver's email address. 127 char max.
						'InvoiceID' => $invoice_id, 											// The invoice number for the payment.  127 char max.
						'PaymentType' => 'SERVICE', 										// Transaction type.  Values are:  GOODS, SERVICE, PERSONAL, CASHADVANCE, DIGITALGOODS
						'PaymentSubType' => '', 									// The transaction subtype for the payment.
						'Phone' => array('CountryCode' => '', 'PhoneNumber' => '', 'Extension' => ''), // Receiver's phone number.   Numbers only.
						'Primary' => 'false'												// Whether this receiver is the primary receiver.  Values are boolean:  TRUE, FALSE
						);
		array_push($Receivers,$Receiver);
		
		$SenderIdentifierFields = array(
										'UseCredentials' => ''						// If TRUE, use credentials to identify the sender.  Default is false.
										);
										
		$AccountIdentifierFields = array(
										'Email' => '', 								// Sender's email address.  127 char max.
										'Phone' => array('CountryCode' => '', 'PhoneNumber' => '', 'Extension' => '')								// Sender's phone number.  Numbers only.
										);
											
		$PayPalRequestData = array(
							'PayRequestFields' => $PayRequestFields, 
							'ClientDetailsFields' => $ClientDetailsFields, 
							'FundingTypes' => $FundingTypes, 
							'Receivers' => $Receivers, 
							'SenderIdentifierFields' => $SenderIdentifierFields, 
							'AccountIdentifierFields' => $AccountIdentifierFields
							);	
							
		$PayPalResult = $this->paypal_adaptive->Pay($PayPalRequestData);
		
		if(!$this->paypal_adaptive->APICallSuccessful($PayPalResult['Ack'])){
			$errors = array('Errors'=>$PayPalResult['Errors']);
			$this->load->view('paypal_error',$errors);
		} else {
			// Successful call.  Load view or whatever you need to do here.



			header('Location: '.$PayPalResult['RedirectURL']);
			exit();
		}
	}

	function payment_success($id){

	}

}

?>