<?php 

class Payments extends CI_Controller{

	function __construct(){
		parent::__construct();
		$this->user_id = $this->session->userdata('user_id');
		$this->load->library('paypal');
	}

	function index(){
		$data['title'] = 'Payments';
		$data['content'] = $this->load->view('payments/payments', $data, TRUE);
		$this->load->view('layouts/default', $data);
	}

	# Get paypal token
	function get_token(){

		$data['emailAddress'] = 'marcpacuri@gmail.com';
		$data['firstName'] = 'Marc';
		$data['lastName'] = 'Pacuri';
		$data['MatchCriteria'] = 'MatchCriteria';
		$data['requestEnvelope'] = 'detailLevel';
		$json = json_encode($data);
		var_dump($this->paypal->verify($json));
		die();
	}

	# Succses page callback
	function success($hash){

		$request = $this->default_db->find_where('payment_requests', array('hash' => $hash));

		if(count($request) > 0){
			$pay['complete'] = 1;
			$this->default_db->save('payment_requests', $pay, $request['id']);

			$workroom = $this->default_db->find('workrooms', $request['workroom_id']);
			$job = $this->default_db->find('jobs', $workroom['job_id']);

			$dt['status'] = 'complete';
			$this->default_db->save('jobs', $dt, $job['id']);
			redirect('employers/dashboard');
		} else{
			redirect('employers/dashboard');
		}
	}


}