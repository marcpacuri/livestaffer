$(document).ready(function(){

	$('#profile_pic').uploadify({
        'swf'      : base_url + 'assets/uploadify/uploadify.swf',
        'uploader' : site_url + 'employees/profile_pic',
        'fileTypeExts' : '*.png; *.jpeg; *.gif; *.jpg',
        'fileSizeLimit' : '5MB',
        'successTimeout' : 90,
        'removeCompleted' : true,
        'queueSizeLimit' : 1,
        'onUploadSuccess' : function(file, data, response) {

            
        },
        'onUploadError': function(file, errorCode, errorMsg, errorString){
            alert(errorString);
        }, 
        'onUploadStart' : function(file) {
             $("#profile_pic").uploadify("settings", "formData", {
                'user_id' : $('#user_id').val()
            });
        }, 
        'onUploadProgress': function(){
	
        }, 
        'onCancel': function(file){
	     
        }
    });
});