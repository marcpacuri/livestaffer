

var table = $('#file-table').dataTable({
		"bProcessing": true,
		"bServerSide": true,
		"bRetrieve": true,
		"bFilter": false,
		// "bLengthChange": false,
		"sAjaxSource": site_url + "files/listings",
		"aoColumns": [
		{ "mData": "created_at", "bSortable": false,  },
		{ "mData": "filename", "bSortable": false,  },
		{ "mData": null, "bSortable": false, }

		], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {

			var actions = "<a href='" + site_url + 'files/download/' + aData.hash + "'>" + 
								"Download" + 
							"</a>";


			$('td:eq(2)', nRow).html(actions);


			return nRow;
		}

});

table.fnDraw();
