notification_check();

function notification_check(){
	$.ajax({
		type: 'post', 
		dataType: 'json', 
		url: site_url + 'employees/notifications', 
		success: function(data){
			set_notifications(data.notifications);
		}
	});
}

function new_notifications(){
	$.ajax({
		type: 'post', 
		dataType: 'json', 
		url: site_url + 'employees/new_notifications', 
		success: function(data){
			set_notifications(data.notifications);
			showNotification();
			$('#notify-badge').show();
		}
	});
}

function notification_read(){
	$.post(site_url + 'employees/notifications_read', function(data){}, 'json');
	$('#notify-badge').hide();
}


$(document).ready(function(){
	$('#notification_btn').click(function(){
		notification_read();
	});

	//broadcast
	if($('#apply_broadcast').val()){
		sendMessageToServer('new_job');
	}
});

function initiate_room(room_id){
	//employer only
}