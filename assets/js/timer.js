var timer; 
var backup; 

$(document).ready(function(){
	socket.on('timer',function(data){

		var message = '';

		switch(data.status){
			case 'start':
				start_time();
				message = 'Time Started';
				break;
			case 'stop':
				stop_time();
				message = 'Time Stopped';
				break;
			case 'employee-start':
				if($('#role').val() == 'employee'){
					start_time();
					socket.emit('timer', {status: 'start'});
				}
		}

		if(message != ''){


			if(window_focus == false){
	            audio.play();

			    var notification = new Notification("LiveStaffer", {
			        dir: "auto",
			        lang: "",
			        body:  message,
			        tag: "",
			    });
				    notification.onshow = function() { setTimeout(function(){notification.cancel()}, 5000) }
				    notification.onerror = function(){
			    }

	        } else{
	        	audio.play();
	        	$.gritter.add({
	                // (string | mandatory) the heading of the notification
	                title: 'LiveStaffer Notification',
	                // (string | mandatory) the text inside the notification
	                text: message, 
	                time: 10000,
	            });
	        }

			
		}

		

	});

	$('#start_time').click(function(){
		socket.emit('timer', {status: 'start'});
	});

	$('#stop_time').click(function(){
		socket.emit('timer', {status: 'stop'});
	});

});

function start_time(){

	if(timer == null || !timer.isActive){
		var hours = parseInt($('#hours').text()); 
		var minutes = parseInt($('#minutes').text());
		var seconds = parseInt($('#seconds').text());
		var total = $('#total_time').val();
		var rate = $('#rate').val();

		timer = $.timer(function(){

			seconds = parseInt(seconds) + 1;

			if(seconds == 60){
				minutes = parseInt(minutes) + 1;
				seconds = 0;
				if(minutes == 60){
					hours = parseInt(hours) + 1; 
					minutes = 0;
				}
			}

			total++;

			$('#hours').text(time_format(hours));
			$('#minutes').text(time_format(minutes));
			$('#seconds').text(time_format(seconds));
			$('#total_time').val(total);

			var realtime_cost = rate * total;
			$('#realtime_cost').text(number_format(realtime_cost, 2, '.', ','));

		}, 1000, true);

		backup = setInterval(function(){save_log()}, 60000);

		socket.emit('timer-change', {'event' : 'start_time'});
	} 

	
}

function stop_time(){

	timer.stop();
	var time = $('#hours').text() + ':' + $('#minutes').text() + ':' + $('#seconds').text();
	var div = $('#log_item').clone();

	$('.date', div).text(moment().format('M/d/YY h:mm A'));
	$('.time', div).text(time);
	// $('.cost', div).text('$' + cost);
	$(div).show();
	$('#logs_table tbody').prepend(div);

	save_log();
	clearInterval(backup);

	socket.emit('timer-change', {'event' : 'stop_time'});
}


function time_format(time){
	if(time < 10){
		time = '0' + time;
	}

	return time;
}

function save_log(){

	if($('#role').val() != 'employee'){
		return false;
	}

	var total_time = $('#total_time').val();
	var cost = number_format($('#rate').val() * $('#total_time').val(), 2, '.', ',');

	$.post(site_url + 'workrooms/log',
	{
		hash: $('#workroom_hash').val(),
		total_time: total_time, 
		cost: cost
	}, function(data){

	}, 'json');

}

window.onbeforeunload = function(e) {
	if(timer.isActive){
		// return 'Please pause the timer before closing the window to save your worklog.';
	}
 
};
