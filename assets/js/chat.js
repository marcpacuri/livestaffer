$(document).ready(function(){

    //JOIN ROOM
    socket.emit('join room', {
        room: $('#workroom_hash').val(), 
        name: $('#username').val(), 
        recipient: $('#recipient').val()
    });

    $('#chat_form').submit(function(){

        var message = $('#chat-input-box').val()
        socket.emit('chat message', message);

        message_in(message);
        log_chat(message);
        scroll_chat();

        $('#chat_form').trigger('reset');
        return false; 
        
    });

    $('#chat-input-box').keyup(function(e){
        if(e.keyCode != 13) return;
        $('#chat_form').trigger('submit');
    })

    socket.on('chat message', function(data){
        message_out(data);
        scroll_chat();
    }); 

    socket.on('chat-status', function(data){
        if(data.status == 'online'){
            $('.status-badge-online').show();
            $('.status-badge-offline').hide();

            // if($('#role').val() == 'employer'){
            //     start_time();
            // }

        } else{
            $('.status-badge-online').hide();
            $('.status-badge-offline').show();

            if($('#role').val() == 'employer'){
                stop_time();
            }
        }
    });

    socket.on('file-reload', function(data){
        table.fnDraw(false);
    });


    //UPLOAD FILE
    $('#share-file').uploadify({
        'swf'      : base_url + 'assets/uploadify/uploadify.swf',
        'uploader' : site_url + 'files/upload',
        'fileTypeExts' : '*',
        'fileSizeLimit' : '5MB',
        'successTimeout' : 90,
        'removeCompleted' : true,
        'queueSizeLimit' : 1,
        'onUploadSuccess' : function(file, data, response) {

            table.fnDraw(false);
            socket.emit('file-reload', 'reload');
           
        },
        'onUploadError': function(file, errorCode, errorMsg, errorString){
            alert(errorString);
        }, 
        'onUploadStart' : function(file) {
             $("#share-file").uploadify("settings", "formData", {
                'workroom_id' : $('#workroom_id').val()
            });
        }, 
        'onUploadProgress': function(){
        
        }, 
        'onCancel': function(file){
         
        }
    });

    // Initiate Call
    $('#btn-share-call').click(function(){
        socket.emit('call-initiate', {
            username: $('#username').val(), 
            type : 'audio'
        });

        $('#call_status').text('Calling...').show();
    });

    $('#btn-share-video').click(function(){
        socket.emit('call-initiate', {
            username: $('#username').val(), 
            type : 'video'
        });

        $('#call_status').text('Calling...').show();
    });

    socket.on('call-initiate', function(data){
        $('#answer').modal('show');

        if(data.type == 'audio'){
            $('#answer_call').show();
            $('#answer_video_call').hide();
        } else{
            $('#answer_call').hide();
            $('#answer_video_call').show();
        }
    });
    
    // Decline Call
    $('#decline_call').click(function(data){
        socket.emit('call-declined', {
            username: $('#username').val()
        });
    });  

    socket.on('call-declined', function(data){
        alert('Call Declined');
        $('#call_status').hide();
    });

    // Answer Call
    $('#answer_call').click(function(data){
        $('#answer').modal('hide');

        socket.emit('call-answer', {
            username: $('#username').val(), 
            type : 'audio'
        });

        start_webrtc();
    });  

    $('#answer_video_call').click(function(data){
        socket.emit('call-answer', {
            username: $('#username').val(), 
            type : 'video'
        });
        start_webrtc();
    });  

    socket.on('call-answer', function(data){
        $('#call_status').hide();

        if(data.type == 'audio'){
            $('.videos-container').hide();
            // connection.addStream({
            //     audio: true,
            //     // video: true
            // });
        } else{
            // connection.addStream({
            //     audio: true,
            //     video: true
            // });
        }
        
        start_webrtc();

    });

    // End Cal
    $('#end_call').click(function(){
        // connection.leave();
        end_webrtc();
        socket.emit('call-end', {username: $('#username').val()});
    });

    socket.on('call-end', function(data){
        // connection.leave();
        end_webrtc();
    });
});


function message_in(message, datetime, load_previous){
    var div = $('#message_in').clone();

    if(!datetime){
        datetime = moment().format('h:mm A');
    } 

    $('.body', div).text(message);
    $('.datetime', div).text(datetime);
    $(div).show();

    if(load_previous){
        $('.chat-output-panel .chats').prepend(div);
    } else{
        $('.chat-output-panel .chats').append(div);
    }
    
    
    // $('.chat-output-panel').scrollTop($('.chat-output-panel').height() + 1000);
}

var audio  =  $('body').find('#pop_mp3').get(0);
var notification;

function chat_message(username, message) {

     audio.play();

    var notification = new Notification("LiveStaffer: " + 'You have a new message', {
        dir: "auto",
        lang: "",
        body:  username + ": " + message,
        tag: "",
    });


    // notification.onclose =
    // notification.onshow = 
    notification.onshow = function() { setTimeout(function(){notification.cancel()}, 5000) }
    notification.onerror = function(){
        // alert('Go to Notification Settings to allow notifications');
    }

}

var window_focus;

$(document).ready(function(){
    $(window).focus(function() {
    window_focus = true;
    }).blur(function() {
        window_focus = false;
    });
});


function message_out(message, datetime, load_previous){
    var div = $('#message_out').clone();

     if(!datetime){
        datetime = moment().format('h:mm A');
    } 

    $('.body', div).text(message);
    $('.datetime', div).text(datetime);
    $(div).show();
    $('.chat-output-panel .chats').prepend(div);
    
    if(load_previous){
        $('.chat-output-panel .chats').prepend(div);
    } else{
        
        if(window_focus == false){
            chat_message($('#recipient').val(), message);
        }
        $('.chat-output-panel .chats').append(div);
    }
}

function log_chat(message){
    $.post(site_url + 'workrooms/chat_log',
    {
        message: message,
        workroom_id: $('#workroom_id').val()
    }, function(data){}, 'json');
}

function chat_logs(){
     var workroom_id = $('#workroom_id').val();
     var role = $('#role').val();
     var index = $('#chat_index').val();

     $('#chat_preloader').show();

    $.post(site_url + 'workrooms/chat_logs/', 
    {
        workroom_id: workroom_id, 
        index: index
    },
    function(data){
       
        $.each(data.messages, function(i, v){

            if(role == 'employee'){
                if(v.role == 'employee'){
                    message_in(v.message, v.created_at, true);
                } else{
                    message_out(v.message, v.created_at, true);
                }
            } else{
                if(v.role == 'employer'){
                    message_in(v.message, v.created_at, true);
                } else{
                    message_out(v.message, v.created_at, true);
                }
            }

        });

        $('#chat_index').val(data.chat_index);

        if(index == 0){
           scroll_chat();
        }

        $('#chat_preloader').hide();


    }, 'json');

 
}

chat_logs();

function scroll_chat(){
    $('.chat-output-panel').scrollTop($('.chat-output-panel').height() + 1000000);
}

$(document).ready(function() {
    $('.chat-output-panel').scroll(function(){
        if  ($('.chat-output-panel').scrollTop() == $('.chat-output-panel').height() - $('.chat-output-panel').height()){
           chat_logs();
        }
    });
});


