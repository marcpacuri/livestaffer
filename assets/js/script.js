$(document).ready(function(){


	$("#authorize").click(function(){
		authorizeNotification();
	});

	$('.delete-this').click(function(){
		if(!confirm('Are you sure?')){
			return false;
		}
	});

	$('#use_reg_details').change(function(){
		if($(this).is(':checked')){
			$('input[name=paypal_email]').val($('input[name=email]').val());
			$('input[name=firstName]').val($('input[name=firstname]').val());
			$('input[name=lastName]').val($('input[name=lastname]').val());
		} else{
			$('input[name=paypal_email]').val('');
			$('input[name=firstName]').val('');
			$('input[name=lastName]').val('');
		}
	});

});

var havePermission = window.webkitNotifications.checkPermission();

if (havePermission != 0) { 
	$('#authorize_modal').modal('show');
}


function authorizeNotification() {
	Notification.requestPermission(function(perm) {
		// alert(perm);
	});
}

function showNotification(message) {
	var notification = new Notification("LiveStaffer: " + 'You have new notifications', {
		dir: "auto",
		lang: "",
		body: "Please check your dashboard",
		tag: "",
	});

// notification.onclose =
// notification.onshow = 
notification.onshow = function() { setTimeout(function(){notification.cancel()}, 5000) }
	notification.onerror = function(){
		// alert('Go to Notification Settings to allow notifications');
	}

}

function set_notifications(notifications){

	var count_new = 0;

	$.each(notifications, function(i, v){

		var div = $('#notify').clone();

		if(v.status == 0){
			$('a', div).addClass('new-notification');
			count_new++;
		}

		
		$('.link', div).attr('href', v.link);
		$('.message', div).html(v.message);
		$('.time', div).html(v.time);
		$(div).show();
		$('#notification-list').prepend(div);
	});

	if(count_new > 0){
		$('#notify_number').text(count_new);
	}
}

function number_format(number, decimals, dec_point, thousands_sep) {

  number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
  var n = !isFinite(+number) ? 0 : +number,
    prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
    sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
    dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
    s = '',
    toFixedFix = function (n, prec) {
      var k = Math.pow(10, prec);
      return '' + Math.round(n * k) / k;
    };
  // Fix for IE parseFloat(0.55).toFixed(0) = 0;
  s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
  if (s[0].length > 3) {
    s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
  }
  if ((s[1] || '').length < prec) {
    s[1] = s[1] || '';
    s[1] += new Array(prec - s[1].length + 1).join('0');
  }
  return s.join(dec);
}


