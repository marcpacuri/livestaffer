// Create SocketIO instance

switch($('#environment').val()){
    case 'development':
        var socket = io.connect("http://localhost", {port: 8080});
        break;
    case 'testing':
    case 'production':
        var socket = io.connect("http://live_signal.jit.su");
        break;
}


// var socket = new io.Socket('localhost',{
//     port: 8080
// });
// socket.connect(); 

// Add a connect listener
socket.on('connect',function() {
    log('<span style="color:green;">Client has connected to the server!</span>');
});
// Add a connect listener
socket.on('message',function(data) {
    log('Received a message from the server:  ' + data); 
    new_notifications();
});
// Add a disconnect listener
socket.on('disconnect',function() {
    log('<span style="color:red;">The client has disconnected!</span>');
});

// Sends a message to the server via sockets
function sendMessageToServer(message) {
    socket.send(message);
    log('<span style="color:#888">Sending "' + message + '" to the server!</span>');
}

// Outputs to console and list
function log(message) {
    var li = document.createElement('li');
    li.innerHTML = message;
    // document.getElementById('message-list').appendChild(li);
}
/*
// Create a socket instance
socket = new WebSocket('ws://localhost:8080');

// Open the socket
socket.onopen = function(event) {
    console.log('Socket opened on client side',event);
    
    // Listen for messages
    socket.onmessage = function(event) {
        console.log('Client received a message',event);
    };
    
    // Listen for socket closes
    socket.onclose = function(event) {
        console.log('Client notified socket has closed',event);
    };
    
};
*/
