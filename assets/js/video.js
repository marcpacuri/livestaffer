
function openSignalingChannel(config) {
    config.channel = config.channel || this.channel || location.hash.substr(1) || 'admin-guest-audio-video-calling';
            var websocket = new WebSocket('wss://www.webrtc-experiment.com:8563');
            websocket.channel = config.channel;
            websocket.onopen = function() {
                websocket.push(JSON.stringify({
                    open: true,
                    channel: config.channel
                }));
                if (config.callback) config.callback(websocket);
            };
            websocket.onmessage = function(event) {
                config.onmessage(JSON.parse(event.data));
            };
            websocket.push = websocket.send;
            websocket.send = function(data) {
                websocket.push(JSON.stringify({
                    data: data,
                    channel: config.channel
                }));

            };
}

(function () {
        var params = {},
                r = /([^&=]+)=?([^&]*)/g;

        function d(s) {
            return decodeURIComponent(s.replace(/\+/g, ' '));
        }

        var match, search = window.location.search.toLowerCase();
        while (match = r.exec(search.substring(1)))
            params[d(match[1])] = d(match[2]);

        window.params = params;
    })();
    
if(!params.joinroom && !params.openroom) {
    // location.href = location.href + '?openRoom=' + (Math.random() * 10000).toString().replace('.', '');
}



//----------------------------------------

var connection = new RTCMultiConnection('abcdef-98765-xyz');
connection.session = {
    data: true
};
connection.openSignalingChannel = openSignalingChannel;

//----------------------------------------
// DATA CHANNEL
connection.onopen = function(e) {
    ('Data connection is opened between you and ' + e.userid + '.');
    chatInputBox.disabled = false;
    shareFile.disabled = false;
    btnShareScreen.disabled = false;
    btnShareVideo.disabled = false;
    btnShareCall.disabled = false;
    btnAnswerCall.disabled = false;
    btnAnswerVideoCall.disabled = false;
    message_out('Data connection is opened between you and ' + e.userid + '.');
    // start_time();
};

connection.onmessage = function(e) {
    appendTextMessage(e.data);
    message_out(e.data);
    scroll_chat();
};

// show progress bar!
// connection.onFileProgress = function (packets, uuid) {
//     // packets.remaining
//     // packets.sent
//     // packets.received
//     // packets.length

//     // uuid: file unique identifier
//     appendFileProgressMessage(packets.remaining + ' remaining...');
// };

// on file successfully sent
// connection.onFileSent = function (file, uuid) {
//     // file.name
//     // file.size
//     appendFileProgressMessage(file.name + ' sent.');
// };

// // on file successfully received
// connection.onFileReceived = function (fileName, file) {
//     // file.blob
//     // file.dataURL
//     // file.url
//     // file.uuid
//     appendFileProgressMessage(fileName + ' received.');
// };

//----------------------------------------
// MEDIA
connection.onstream = function(e) {
    //if(e.type == 'local') return;
    videosContainer.insertBefore(e.mediaElement, videosContainer.firstChild);
};

connection.onstreamended = function(e) {
    videosContainer.removeChild(e.mediaElement);
};

//----------------------------------------
// HELPERS
function appendTextMessage(message) {
    // var div = document.createElement('div');
    // div.innerHTML = message;
    // chatOutputPanel.insertBefore(div, chatOutputPanel.firstChild);
    
}
function appendFileProgressMessage(message) {
    // var div = document.createElement('div');
    // div.innerHTML = message;
    // // fileProgressPreviewer.insertBefore(div, fileProgressPreviewer.firstChild);
    // $('.chat-output-panel').append(message);
}

//----------------------------------------
// UI
function getEelement(selector) {return document.querySelector(selector);}

var chatOutputPanel = getEelement('.chat-output-panel'),
    chatInputBox = getEelement('#chat-input-box'),
    
    btnShareVideo = getEelement('#btn-share-video'),
    btnShareCall = getEelement('#btn-share-call'),
    btnShareScreen = getEelement('#btn-share-screen'),

    btnAnswerCall = getEelement('#answer_call'),
    btnAnswerVideoCall = getEelement('#answer_video_call'),
        
    videosContainer = getEelement('.videos-container'),
    
    shareFile = getEelement('#share-file'),
    fileProgressPreviewer = getEelement('.file-progress-previewer');
    
btnAnswerVideoCall.onclick = function() {
    // this.disabled = true;
    connection.addStream({
        audio: true,
        video: true
    });
};

btnAnswerCall.onclick = function() {
    // this.disabled = true;
    connection.addStream({
        audio: true,
        // video: true
    });
};

// btnShareScreen.onclick = function() {
//     this.disabled = true;
//     connection.addStream({
//         screen: true,
//         oneway: true
//     });
// };

chatInputBox.onkeyup = function(e) {
    // if(e.keyCode != 13) return;
    // var value = this.value;
    // this.value = '';
    
    // connection.send(value);
    // appendTextMessage(value);
    // message_in(value);
    // log_chat(value);
    // scroll_chat();
};

shareFile.onchange = function() {
    var file = this.files[0];
    connection.send(file);
};

//----------------------------------------
// SETUP

if(params.joinroom)
{
    connection.userid = 'employee';
    connection.connect(params.joinroom);

}
if(params.openroom) {
    connection.userid = 'employer';
    connection.open($('#workroom_hash').val()); 

    var h1 = document.createElement('h1');

    if($('#new_workroom').val() == 1){
        initiate_room($('#workroom_hash').val());
    }

    // h1.innerHTML = '<a href="' + location.href.replace('openRoom', 'joinRoom') + '" target="_blank">Click Here to Join Room in NEW Tab</a></h1>';
    // document.body.insertBefore(h1, document.body.firstChild);

    var notify = '<a href="' + location.href.replace('openRoom', 'joinRoom') + '" target="_blank">Click Here to Join Room in NEW Tab</a></h1>';
   
    $('#room_notify').html(notify);
}

//----------------------------------------


