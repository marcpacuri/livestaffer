notification_check();

function notification_check(){
	$.ajax({
		type: 'post', 
		dataType: 'json', 
		url: site_url + 'employers/notifications', 
		success: function(data){
			set_notifications(data.notifications);
		}
	});
}

function new_notifications(){
	$.ajax({
		type: 'post', 
		dataType: 'json', 
		url: site_url + 'employers/new_notifications', 
		success: function(data){
			set_notifications(data.notifications);
			showNotification();
			$('#notify-badge').show();
		}
	});
}

function notification_read(){
	$.post(site_url + 'employers/notifications_read', function(data){}, 'json');
	$('#notify-badge').hide();
}

function initiate_room(room_id){
	$.post(site_url + 'workrooms/notify_employee/', 
	{
		job_id: $('#job_id').val(),
		employee_id: $('#employee_id').val(), 
		employer_id: $('#employer_id').val(), 
		room_id: room_id
	}, function(data){
		sendMessageToServer('new_job');
	}, 'json');
}

$(document).ready(function(){
	$('#notification_btn').click(function(){
		notification_read();
	});

	//broadcast
	if($('#new_job').val()){
		sendMessageToServer('new_job');
	}
});



